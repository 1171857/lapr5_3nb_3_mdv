﻿using System.Threading.Tasks;
using System.Collections.Generic;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Drivers;
using DDDSample1.Dtos.Drivers;
using DDDSample1.Services.IRepositories;

namespace DDDSample1.Services
{
    public class DriverService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IDriverRepository _repo;

        public DriverService(IUnitOfWork unitOfWork, IDriverRepository repo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
        }

        public async Task<List<DriverDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();

            List<DriverDto> listDto = list.ConvertAll<DriverDto>(driver =>
                new DriverDto(driver.Id.AsGuid(), driver.MechanographicalNumber,
                    driver.Name, driver.BirthDate, driver.Nif, driver.LicenseNumber,
                    driver.LicenseExpirationDate, driver.StartDate,
                    driver.ExitDate, driver.DriverTypes
                )
            );

            return listDto;
        }

        public async Task<DriverDto> GetByIdAsync(DriverId id)
        {
            var driver = await this._repo.GetByIdAsync(id);

            if (driver == null)
                return null;

            return new DriverDto(driver.Id.AsGuid(), driver.MechanographicalNumber,
                driver.Name, driver.BirthDate, driver.Nif, driver.LicenseNumber,
                driver.LicenseExpirationDate, driver.StartDate,
                driver.ExitDate, driver.DriverTypes
            );
        }

        public async Task<DriverDto> AddAsync(CreatingDriverDto dto)
        {
            var driver = new Driver(dto.MechanographicalNumber,
                dto.Name, dto.BirthDate, dto.Nif, dto.LicenseNumber,
                dto.LicenseExpirationDate, dto.StartDate,
                dto.ExitDate, dto.DriverTypes
            );

            await this._repo.AddAsync(driver);

            await this._unitOfWork.CommitAsync();

            return new DriverDto(driver.Id.AsGuid(), driver.MechanographicalNumber,
                driver.Name, driver.BirthDate, driver.Nif, driver.LicenseNumber,
                driver.LicenseExpirationDate, driver.StartDate,
                driver.ExitDate, driver.DriverTypes
            );
        }

        public async Task<DriverDto> UpdateAsync(DriverDto dto)
        {
            var driver = await this._repo.GetByIdAsync(new DriverId(dto.Id));

            if (driver == null)
                return null;

            // change all fields
            // TODO

            await this._unitOfWork.CommitAsync();

            return new DriverDto(driver.Id.AsGuid(), driver.MechanographicalNumber,
                driver.Name, driver.BirthDate, driver.Nif, driver.LicenseNumber,
                driver.LicenseExpirationDate, driver.StartDate,
                driver.ExitDate, driver.DriverTypes
            );
        }

        public async Task<DriverDto> DeleteAsync(DriverId id)
        {
            var driver = await this._repo.GetByIdAsync(id);

            if (driver == null)
                return null;

            this._repo.Remove(driver);
            await this._unitOfWork.CommitAsync();

            return new DriverDto(driver.Id.AsGuid(), driver.MechanographicalNumber,
                driver.Name, driver.BirthDate, driver.Nif, driver.LicenseNumber,
                driver.LicenseExpirationDate, driver.StartDate,
                driver.ExitDate, driver.DriverTypes
            );
        }
    }
}

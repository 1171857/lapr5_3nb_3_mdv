﻿using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.VehicleServices
{
    public interface IVehicleServiceRepository : IRepository<VehicleService, VehicleServiceId>
    {
    }
}
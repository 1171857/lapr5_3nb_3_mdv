
using System;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.WorkBlocks
{
    public interface IWorkBlockRepository : IRepository<WorkBlock, WorkBlockId>
    {
        public Task<WorkBlock> GetByIdentifierAsync(String identifier);
    }
}
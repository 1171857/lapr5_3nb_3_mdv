﻿using DDDSample1.Domain.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDDSample1.Domain.DriverServices
{
    public interface IDriverServiceRepository : IRepository<DriverService, DriverServiceId>
    { 
    }
}
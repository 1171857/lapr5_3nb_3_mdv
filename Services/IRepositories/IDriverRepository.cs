﻿using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Drivers;

namespace DDDSample1.Services.IRepositories
{
    public interface IDriverRepository : IRepository<Driver, DriverId>
    {
    }
}

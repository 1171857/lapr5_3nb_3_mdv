using System.Threading.Tasks;
using System.Collections.Generic;
using DDDSample1.Domain.Shared;
using DDDSample1.Infrastructure.Users;

namespace DDDSample1.Domain.Users
{
    public class UserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserRepository _repo;

        public UserService(IUnitOfWork unitOfWork, IUserRepository repo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
        }

        public async Task<List<UserDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();

            List<UserDto> listDto = list.ConvertAll<UserDto>(user =>
                new UserDto(user.Id.AsGuid(), user.Name, user.Email, user.Password, user.AcceptanceOfPrivacyPolicy, user.LastLogin, user.Role));

            return listDto;
        }

        public async Task<UserDto> GetByIdAsync(UserId id)
        {
            var user = await this._repo.GetByIdAsync(id);

            if (user == null)
                return null;

            return new UserDto(user.Id.AsGuid(), user.Name, user.Email, user.Password, user.AcceptanceOfPrivacyPolicy, user.LastLogin, user.Role);
        }

        public async Task<UserDto> AddAsync(CreatingUserDto dto)
        {
            var user = new User(dto.Name, dto.Email, dto.Password, dto.Role);

            await this._repo.AddAsync(user);

            await this._unitOfWork.CommitAsync();

            return new UserDto(user.Id.AsGuid(), user.Name, user.Email, user.Password, user.AcceptanceOfPrivacyPolicy, user.LastLogin, user.Role);
        }

        public async Task<UserDto> UpdateAsync(UserDto dto)
        {
            var user = await this._repo.GetByIdAsync(new UserId(dto.Id));

            if (user == null)
                return null;

            // change all fields
            
            await this._unitOfWork.CommitAsync();

            return new UserDto(user.Id.AsGuid(), user.Name, user.Email, user.Password, user.AcceptanceOfPrivacyPolicy, user.LastLogin, user.Role);
        }

        public async Task<UserDto> DeleteAsync(UserId id)
        {
            var user = await this._repo.GetByIdAsync(id);

            if (user == null)
                return null;

            this._repo.Remove(user);
            await this._unitOfWork.CommitAsync();

            return new UserDto(user.Id.AsGuid(), user.Name, user.Email, user.Password, user.AcceptanceOfPrivacyPolicy, user.LastLogin, user.Role);
        }

        public bool IsValidUserCredentials(string email, string password)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                return false;
            }

            if (string.IsNullOrWhiteSpace(password))
            {
                return false;
            }
            User user = _repo.GetByEmailAsync(email).Result;
            return BCrypt.Net.BCrypt.Verify(password, user.Password);
        }

        public async Task<string> GetUserRole(string email)
        {
            User user =  await _repo.GetByEmailAsync(email);
            if (user == null)
            {
                return string.Empty;
            }

            return user.Role.ToString();
        }
    }
}
﻿using DDDSample1.Domain.Routes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;
using System.Web;

namespace DDDNetCore.Services
{
    class MdrClientService : IDisposable
    {
        HttpClient client;
        JsonSerializerOptions options;
        const string MDR_HOST = "lapr5-3nb-3-mdr.herokuapp.com";//"127.0.0.1";
        const int MDR_PORT = 8081;
        const string MDR_ROUTE_API = "routes";
        const string MDR_SCHEME = "https";

        public MdrClientService() {

            client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            options = new JsonSerializerOptions
            {
                PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
            };
        }

        public async Task<List<RouteDto>> GetRoutes(string lineCode)
        {
            var builder = new UriBuilder();
            builder.Scheme = MDR_SCHEME;
            builder.Host = MDR_HOST;
            //builder.Port = MDR_PORT;
            builder.Path = MDR_ROUTE_API;

            var query = HttpUtility.ParseQueryString(string.Empty);
            query["lineCode"] = lineCode;

            builder.Query = query.ToString();
            string url = builder.ToString();

            try
            {
                var response = await client.GetAsync(url);
                response.EnsureSuccessStatusCode();

                if (response.IsSuccessStatusCode)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    var result = JsonSerializer.Deserialize<List<RouteDto>>(json, options);
                    return result;
                }
                else
                {
                    throw new Exception("An unexpected error occurred when fetching data from MDR." +
                        "\nStatus Code: " + response.StatusCode + "\nReason: " + response.ReasonPhrase);
                }
            }
            catch (Exception e)
            {
                throw new Exception("An unexpected error occurred when fetching data from MDR." + e.Message);
            }
        }

        public void Dispose()
        {
            if (this.client != null)
            {
                client.Dispose();
            }
        }
    }

}

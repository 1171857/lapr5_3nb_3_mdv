﻿using System.Threading.Tasks;
using System.Collections.Generic;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.Vehicles
{
    public class VehicleService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IVehicleRepository _repo;

        public VehicleService(IUnitOfWork unitOfWork, IVehicleRepository repo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
        }

        public async Task<List<VehicleDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();

            List<VehicleDto> listDto = list.ConvertAll<VehicleDto>(vehicle =>
                new VehicleDto(vehicle.Id.AsGuid(), vehicle.Registration, vehicle.VIN, vehicle.VehicleTypeCode, vehicle.DateOfServiceStart));

            return listDto;
        }

        public async Task<VehicleDto> GetByIdAsync(VehicleId id)
        {
            var vehicle = await this._repo.GetByIdAsync(id);

            if (vehicle == null)
                return null;

            return new VehicleDto(vehicle.Id.AsGuid(), vehicle.Registration, vehicle.VIN, vehicle.VehicleTypeCode, vehicle.DateOfServiceStart);
        }

        public async Task<VehicleDto> AddAsync(CreatingVehicleDto dto)
        {
            var vehicle = new Vehicle(dto.Registration, dto.VIN, dto.VehicleTypeCode, dto.DateOfServiceStart);

            await this._repo.AddAsync(vehicle);

            await this._unitOfWork.CommitAsync();

            return new VehicleDto(vehicle.Id.AsGuid(), vehicle.Registration, vehicle.VIN, vehicle.VehicleTypeCode, vehicle.DateOfServiceStart);
        }

        public async Task<VehicleDto> UpdateAsync(VehicleDto dto)
        {
            var vehicle = await this._repo.GetByIdAsync(new VehicleId(dto.Id));

            if (vehicle == null)
                return null;

            // change all fields
            vehicle.ChangeVehicleTypeCode(dto.VehicleTypeCode);

            await this._unitOfWork.CommitAsync();

            return new VehicleDto(vehicle.Id.AsGuid(), vehicle.Registration, vehicle.VIN, vehicle.VehicleTypeCode, vehicle.DateOfServiceStart);
        }

        public async Task<VehicleDto> DeleteAsync(VehicleId id)
        {
            var vehicle = await this._repo.GetByIdAsync(id);

            if (vehicle == null)
                return null;

            this._repo.Remove(vehicle);
            await this._unitOfWork.CommitAsync();

            return new VehicleDto(vehicle.Id.AsGuid(), vehicle.Registration, vehicle.VIN, vehicle.VehicleTypeCode, vehicle.DateOfServiceStart);
        }
    }
}
﻿using System.Threading.Tasks;
using System.Collections.Generic;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Trips;
using DDDSample1.Domain.WorkBlocks;
using DDDSample1.Domain.DriverServices;
using System.Linq;
using System;

namespace DDDSample1.Domain.DriverServices
{
    public class DriverServiceService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IDriverServiceRepository _repo;
        private readonly ITripRepository _tripRepo;
        private readonly IWorkBlockRepository _workBlockRepo;

        public DriverServiceService(IUnitOfWork unitOfWork, IDriverServiceRepository repo, ITripRepository tripRepo, IWorkBlockRepository workBlockRepo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
            this._tripRepo = tripRepo;
            this._workBlockRepo = workBlockRepo;
        }

        public async Task<List<DriverServiceDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();

            List<DriverServiceDto> listDto = list.ConvertAll<DriverServiceDto>(DriverService =>
                new DriverServiceDto(DriverService.Id.AsGuid(), DriverService.DriverServiceIdentifier, DriverService.StartTime, DriverService.EndTime, DriverService.WorkBlocks));

            return listDto;
        }

        public async Task<DriverServiceDto> GetByIdAsync(DriverServiceId id)
        {
            var DriverService = await this._repo.GetByIdAsync(id);

            if (DriverService == null)
                return null;

            return new DriverServiceDto(DriverService.Id.AsGuid(), DriverService.DriverServiceIdentifier, DriverService.StartTime, DriverService.EndTime, DriverService.WorkBlocks);
        }

        public async Task<DriverServiceDto> AddAsync(CreatingDriverServiceDto dto)
        {
            ICollection<WorkBlock> workblocks = new List<WorkBlock>();
            foreach (WorkBlockId element in dto.ListWorkBlocks)
            {
                var wb = _workBlockRepo.GetByIdAsync(element).Result;

                if (wb == null)
                {
                    throw new BusinessRuleValidationException("Invalid workblock!");
                }
                else
                {
                    workblocks.Add(wb);
                }
            }

            // Get total workblock time
            var totalWBTime = 0;
            var _8Hours = 28_800;
            
            foreach (var wb in workblocks)
            {
                var start = wb.StartTime;
                var end = wb.EndTime;
                if (wb.EndTime < wb.StartTime)
                {
                    end += 86400;
                }
                totalWBTime += (end - start);
            }

            if (totalWBTime > _8Hours) 
            {
                var timeSpan = TimeSpan.FromSeconds(totalWBTime);
                var friendlyTime = string.Format("{0:D2}h:{1:D2}m:{2:D2}s:{3:D3}ms",timeSpan.Hours,timeSpan.Minutes,timeSpan.Seconds,timeSpan.Milliseconds);
                throw new BusinessRuleValidationException("The total time of the selected workblocks ("+ friendlyTime+") exceeds the 8 hours maximum!");
            }

            /*if (this.AreWorkBlocksbIntersecting(workblocks.ToList()))
            { 
                throw new BusinessRuleValidationException("One or more of the provided workblocks have intersecting times!");
            }*/

            var DriverService = new DriverService(dto.DriverServiceIdentifier, dto.StartTime, dto.EndTime, workblocks);

            if (dto.ListWorkBlocks.Count > 0)
            {
                DriverService.AddWorkBlocks(workblocks);
            }

            await this._repo.AddAsync(DriverService);

            await this._unitOfWork.CommitAsync();

            return new DriverServiceDto(DriverService.Id.AsGuid(), DriverService.DriverServiceIdentifier, DriverService.StartTime, DriverService.EndTime, workblocks);
        }

        private bool AreWorkBlocksbIntersecting(List<WorkBlock> workblocks)
        {
            if (workblocks != null && workblocks.Count > 1)
            {
                for (var i = 0; i < workblocks.Count - 1; i++)
                {
                    for (var j = 1; j < workblocks.Count; j++)
                    {
                        var a = workblocks[i];
                        var b = workblocks[j];

                        // Assuming all workblocks are valid (their start is lower than their end)
                        if (
                          (a.StartTime > b.StartTime && a.StartTime < b.EndTime)
                          ||
                          (a.EndTime > b.StartTime && a.EndTime < b.EndTime)
                          ||
                          (b.StartTime > a.StartTime && b.StartTime < a.EndTime)
                          ||
                          (b.EndTime > a.StartTime && b.EndTime < a.EndTime)
                          )
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public async Task<DriverServiceDto> UpdateAsync(DriverServiceDto dto)
        {
            var DriverService = await this._repo.GetByIdAsync(new DriverServiceId(dto.Id));

            if (DriverService == null)
                return null;

            // change all fields


            await this._unitOfWork.CommitAsync();

            return new DriverServiceDto(DriverService.Id.AsGuid(), DriverService.DriverServiceIdentifier, DriverService.StartTime, DriverService.EndTime, DriverService.WorkBlocks);
        }

        public async Task<DriverServiceDto> DeleteAsync(DriverServiceId id)
        {
            var DriverService = await this._repo.GetByIdAsync(id);

            if (DriverService == null)
                return null;

            this._repo.Remove(DriverService);
            await this._unitOfWork.CommitAsync();

            return new DriverServiceDto(DriverService.Id.AsGuid(), DriverService.DriverServiceIdentifier, DriverService.StartTime, DriverService.EndTime, DriverService.WorkBlocks);
        }
    }
}
﻿using System.Threading.Tasks;
using System.Collections.Generic;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Trips;
using System;
using DDDSample1.Domain.Routes;
using DDDNetCore.Services;
using System.Linq;
using Microsoft.AspNetCore.Http;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using DDDSample1.Controllers;

namespace DDDSample1.Services
{
    public class FileService
    {
        private readonly TripService _tripService;

        public FileService(TripService tripService)
        {
            _tripService = tripService;
        }

        public async Task<Boolean> ImportXMLAsync(IFormFile xmlFile)
        {
            var xml = new XmlDocument();
            xml.Load(xmlFile.OpenReadStream());
            string jsonText = JsonConvert.SerializeXmlNode(xml, Newtonsoft.Json.Formatting.Indented);
            string jsonString = Regex.Replace(jsonText, "(?<=\")(@)(?!.*\":\\s )", string.Empty, RegexOptions.IgnoreCase);
            dynamic stuff = JObject.Parse(jsonString);
            dynamic trips = stuff.GlDocumentInfo.world.GlDocument.GlDocumentSchedule.Schedule.Trips;
            ImportTrips(trips);
            return true;
        }

        public async Task<Boolean> ImportTrips(dynamic trips)
        {
            //Console.WriteLine(trips);
            int tripsCount = trips.Trip.Count;
            for (int i = 0; i < tripsCount; i++)
            {
                List<CreatingScheduleDto> schedule = new List<CreatingScheduleDto>();
                dynamic passingTimes = trips.Trip[i].PassingTimes.PassingTime;
                int passingTimeCount = passingTimes.Count;
                for (int j = 0; j < passingTimeCount; j++)
                {
                    String node = passingTimes[j].Node;
                    int time = passingTimes[j].Time;
                    CreatingScheduleDto scheduleDto = new CreatingScheduleDto(node, time);
                    schedule.Add(scheduleDto);
                }
                string id = trips.Trip[i].key;
                string orientation = trips.Trip[i].Orientation;
                string line;
                try
                {
                    line = trips.Trip[i].Line;
                }
                catch
                {
                    line = "";
                }
                string route = trips.Trip[i].Path;
                CreatingTripDto tripDto = new CreatingTripDto(id, orientation, line, route, schedule);
                var trip = await _tripService.AddAsync(tripDto);
            }
            return true;
        }
    }
}
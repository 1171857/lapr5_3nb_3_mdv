﻿using System.Threading.Tasks;
using System.Collections.Generic;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Trips;
using DDDSample1.Domain.WorkBlocks;

namespace DDDSample1.Domain.VehicleServices
{
    public class VehicleServiceService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IVehicleServiceRepository _repo;
        private readonly ITripRepository _tripRepo;
        private readonly IWorkBlockRepository _workBlockRepo;

        public VehicleServiceService(IUnitOfWork unitOfWork, IVehicleServiceRepository repo, ITripRepository tripRepo, IWorkBlockRepository workBlockRepo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
            this._tripRepo = tripRepo;
            this._workBlockRepo = workBlockRepo;
        }

        public async Task<List<VehicleServiceDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();

            List<VehicleServiceDto> listDto = list.ConvertAll<VehicleServiceDto>(vehicleService =>
                new VehicleServiceDto(vehicleService.Id.AsGuid(), vehicleService.VehicleServiceIdentifier, vehicleService.StartTime, vehicleService.EndTime, vehicleService.Trips, vehicleService.WorkBlocks));

            return listDto;
        }

        public async Task<VehicleServiceDto> GetByIdAsync(VehicleServiceId id)
        {
            var vehicleService = await this._repo.GetByIdAsync(id);

            if (vehicleService == null)
                return null;

            return new VehicleServiceDto(vehicleService.Id.AsGuid(), vehicleService.VehicleServiceIdentifier, vehicleService.StartTime, vehicleService.EndTime, vehicleService.Trips, vehicleService.WorkBlocks);
        }

        public async Task<VehicleServiceDto> AddAsync(CreatingVehicleServiceDto dto)
        {
            List<Trip> Trips = new List<Trip>();

            if (dto.ListTrips != null)
            {
                foreach (TripId element in dto.ListTrips)
                {
                    var trip = _tripRepo.GetByIdAsync(element).Result;
                    if (trip == null)
                    {
                        throw new BusinessRuleValidationException("Invalid TripId!");
                    }
                    else
                    {
                        Trips.Add(trip);
                    }
                }
            }

            ICollection<Trip> TripCollection = Trips;
            ICollection<WorkBlock> workblocks = new List<WorkBlock>();
            foreach (WorkBlockId element in dto.ListWorkBlocks)
            {
                var wb = _workBlockRepo.GetByIdAsync(element).Result;
                if (wb == null)
                {
                    throw new BusinessRuleValidationException("Invalid workblock!");
                }
                else
                {
                    workblocks.Add(wb);
                }
            }

            var vehicleService = new VehicleService(dto.VehicleServiceIdentifier, dto.StartTime, dto.EndTime, TripCollection);

            if (dto.ListWorkBlocks.Count > 0)
            {
                vehicleService.AddWorkBlocks(workblocks);
            }

            await this._repo.AddAsync(vehicleService);

            await this._unitOfWork.CommitAsync();

            return new VehicleServiceDto(vehicleService.Id.AsGuid(), vehicleService.VehicleServiceIdentifier, vehicleService.StartTime, vehicleService.EndTime, vehicleService.Trips, workblocks);
        }

        public async Task<VehicleServiceDto> UpdateAsync(VehicleServiceDto dto)
        {
            var vehicleService = await this._repo.GetByIdAsync(new VehicleServiceId(dto.Id));

            if (vehicleService == null)
                return null;

            // change all fields
            

            await this._unitOfWork.CommitAsync();

            return new VehicleServiceDto(vehicleService.Id.AsGuid(), vehicleService.VehicleServiceIdentifier, vehicleService.StartTime, vehicleService.EndTime, vehicleService.Trips, vehicleService.WorkBlocks);
        }

        public async Task<VehicleServiceDto> DeleteAsync(VehicleServiceId id)
        {
            var vehicleService = await this._repo.GetByIdAsync(id);

            if (vehicleService == null)
                return null;

            this._repo.Remove(vehicleService);
            await this._unitOfWork.CommitAsync();

            return new VehicleServiceDto(vehicleService.Id.AsGuid(), vehicleService.VehicleServiceIdentifier, vehicleService.StartTime, vehicleService.EndTime, vehicleService.Trips, vehicleService.WorkBlocks);
        }
    }
}
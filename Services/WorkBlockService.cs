using System.Threading.Tasks;
using System.Collections.Generic;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Trips;
using DDDSample1.Domain.VehicleServices;
using System;
using System.Linq;
using DDDSample1.Domain.DriverServices;

namespace DDDSample1.Domain.WorkBlocks
{
    public class WorkBlockService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWorkBlockRepository _repo;
        private readonly ITripRepository _tripRepo;
        private readonly IVehicleServiceRepository _vehicleServiceRepo;
        private readonly IDriverServiceRepository _driverServiceRepo;

        public WorkBlockService(IUnitOfWork unitOfWork, IWorkBlockRepository repo, ITripRepository tripRepo, IVehicleServiceRepository vehicleServiceRepo, IDriverServiceRepository driverServiceRepo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
            this._tripRepo = tripRepo;
            this._vehicleServiceRepo = vehicleServiceRepo;
            this._driverServiceRepo = driverServiceRepo;
        }

        public async Task<List<WorkBlockDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();

            List<WorkBlockDto> listDto = list.ConvertAll<WorkBlockDto>(workBlock => 
            new WorkBlockDto(workBlock.Id.AsGuid(), workBlock.WorkBlockIdentifier, workBlock.StartTime, workBlock.EndTime, workBlock.Trips, workBlock.VehicleService));

            return listDto;
        }

        public async Task<List<WorkBlockDto>> GetAssignableAsync()
        {
            var list = await this._repo.GetAllAsync();
            var driverServices = await this._driverServiceRepo.GetAllAsync();
            var assignedWorkblocks = driverServices.SelectMany(w => w.WorkBlocks).Select(w => w.Id).Distinct();

            List<WorkBlockDto> listDto = list.Where(w => w.VehicleService != null && !assignedWorkblocks.Contains(w.Id))
                .ToList()
                .ConvertAll(workBlock =>
                    new WorkBlockDto(
                        workBlock.Id.AsGuid(), 
                        workBlock.WorkBlockIdentifier, 
                        workBlock.StartTime, 
                        workBlock.EndTime, 
                        workBlock.Trips, 
                        workBlock.VehicleService));

            return listDto;
        }

        public async Task<WorkBlockDto> GetByIdAsync(WorkBlockId id)
        {
            var workBlock = await this._repo.GetByIdAsync(id);

            if (workBlock == null)
                return null;

            return new WorkBlockDto(workBlock.Id.AsGuid(), workBlock.WorkBlockIdentifier, workBlock.StartTime, workBlock.EndTime, workBlock.Trips, workBlock.VehicleService);
        }

        public async Task<WorkBlockDto> GetByIdentifierAsync(String identifier)
        {
            var workBlock = await this._repo.GetByIdentifierAsync(identifier);

            if (workBlock == null)
                return null;

            return new WorkBlockDto(workBlock.Id.AsGuid(), workBlock.WorkBlockIdentifier, workBlock.StartTime, workBlock.EndTime, workBlock.Trips, workBlock.VehicleService);
        }

        public async Task<WorkBlockDto> AddAsync(CreatingWorkBlockDto dto)
        {
            List<Trip> Trips = new List<Trip>();
            VehicleService vehicleService = null;
            if (dto.VehicleServiceId != null)
            {
                vehicleService = _vehicleServiceRepo.GetByIdAsync(dto.VehicleServiceId).Result;
                if (vehicleService == null)
                {
                    throw new BusinessRuleValidationException("Invalid VehicleServiceId!");
                }
            }

            foreach (TripId element in dto.ListTrips) // TESTAR PARA UM ID QUE N�O EXISTE! 

            {
                var trip = _tripRepo.GetByIdAsync(element).Result;
                if(trip == null)
                {
                    throw new BusinessRuleValidationException("Invalid TripId!");
                }
                else
                {
                    Trips.Add(trip);
                }
            }

            ICollection<Trip> TripCollection = Trips;

            var workBlock = new WorkBlock(dto.WorkBlockIdentifier, dto.StartTime, dto.EndTime, TripCollection, vehicleService);

            await this._repo.AddAsync(workBlock);

            await this._unitOfWork.CommitAsync();

            return new WorkBlockDto(workBlock.Id.AsGuid(), workBlock.WorkBlockIdentifier, workBlock.StartTime, workBlock.EndTime, workBlock.Trips, workBlock.VehicleService);
        }

            public async Task<WorkBlockDto> UpdateAsync(WorkBlockDto dto)
        {
            var workBlock = await this._repo.GetByIdAsync(new WorkBlockId(dto.Id));

            if (workBlock == null)
                return null;

            // change all field
            workBlock.ChangeStartTime(dto.StartTime);
            workBlock.ChangeEndTime(dto.EndTime);
            //workBlock.ChangeListTrips(dto.ListTrips);

            await this._unitOfWork.CommitAsync();

            return new WorkBlockDto(workBlock.Id.AsGuid(), workBlock.WorkBlockIdentifier, workBlock.StartTime, workBlock.EndTime, workBlock.Trips, workBlock.VehicleService);
        }

        public async Task<WorkBlockDto> DeleteAsync(WorkBlockId id)
        {
            var workBlock = await this._repo.GetByIdAsync(id);

            if (workBlock == null)
                return null;

            this._repo.Remove(workBlock);
            await this._unitOfWork.CommitAsync();

            return new WorkBlockDto(workBlock.Id.AsGuid(), workBlock.WorkBlockIdentifier, workBlock.StartTime, workBlock.EndTime, workBlock.Trips, workBlock.VehicleService);
        }
    }
}
﻿using System.Threading.Tasks;
using System.Collections.Generic;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Trips;
using System;
using DDDSample1.Domain.Routes;
using DDDNetCore.Services;
using System.Linq;

namespace DDDSample1.Services
{
    public class TripService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly ITripRepository _repo;
        private const int SECONDS_IN_DAY = 86400;
        private const int MINUTES_IN_DAY = 1440;

        public TripService(IUnitOfWork unitOfWork, ITripRepository repo)
        {
            this._unitOfWork = unitOfWork;
            this._repo = repo;
        }

        //TODO
        public async Task<List<TripDto>> GetAllAsync()
        {
            var list = await this._repo.GetAllAsync();

            List<TripDto> listDto = list.ConvertAll<TripDto>(trip =>
                new TripDto(trip.Id.AsGuid(), trip.TripIdentifier, trip.LineId, trip.RouteId, trip.Schedules));

            return listDto;
        }

        //TODO
        public async Task<TripDto> GetByIdAsync(TripId id)
        {
            var trip = await this._repo.GetByIdAsync(id);

            if (trip == null)
                return null;

            return new TripDto(trip.Id.AsGuid(), trip.TripIdentifier, trip.LineId, trip.RouteId, trip.Schedules);
        }

        public async Task<TripDto> GetByIdentifierAsync(String identifier)
        {
            var trip = await this._repo.GetByIdentifierAsync(identifier);

            if (trip == null)
                return null;

            return new TripDto(trip.Id.AsGuid(), trip.TripIdentifier, trip.LineId, trip.RouteId, trip.Schedules);
        }

        //TODO
        public async Task<TripDto> AddAsync(CreatingTripDto dto)
        {
            var trip = new Trip(dto.TripIdentifier, dto.LineId, dto.RouteId, dto.Schedules);

            await this._repo.AddAsync(trip);

            try
            {
                await this._unitOfWork.CommitAsync();
            }catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return new TripDto(trip.Id.AsGuid(), trip.TripIdentifier, trip.LineId, trip.RouteId, trip.Schedules);
        }

        //TODO
        public async Task<TripDto> UpdateAsync(TripDto dto)
        {
            var trip = await this._repo.GetByIdAsync(new TripId(dto.Id));

            if (trip == null)
                return null;

            var dbTrip = await this._repo.GetByIdAsync(new TripId(dto.Id));

            if (dbTrip != null)
            {
                dbTrip.ChangeRouteId(dto.RouteId);
                dbTrip.ChangeSchedules(dto.Schedule);
                dbTrip.ChangeTripIdentifier(dto.TripIdentifier);
            }
            else return null;

            await this._unitOfWork.CommitAsync();

            return new TripDto(trip.Id.AsGuid(), trip.TripIdentifier, trip.LineId, trip.RouteId, trip.Schedules);
        }

        /// <summary>
        /// The following methos generates 1 or more trips based on provided parameters.
        /// If SingleTrip == true, 1 trip will be generated
        /// If NTrips == 0, a maximum number of trips will be generated until the endtime is reached
        /// If NTrips > 0, [NTrips] trips will be generated.
        /// If Freq == 0, The next trip will depart rigth when the previous one arrives back
        /// If Freq > 0, The next trip will depart [freq] seconds after the previous one.
        /// If MaxVeichlesInParallel > 0, the number of concurrent trips happening will be dictated by MaxVeichlesInParallel
        /// NOTES
        /// It is assumed that for multiple route generation, there will always be a GO and a RETURN route
        /// It is assumed that the end time specifies the time that the LAST OUTGOING can depart
        /// The time that a bus departs from one node to another is not currently accounting for any stop time. 
        /// </summary>
        /// <param name="dto"></param>
        /// <returns></returns>
        public async Task<List<TripDto>> GenerateTrips(TripGenerationDto dto)
        {
            var trips2Add = new List<TripDto>();
            RouteDto singleRoute = null;
            RouteDto outwardRoute = null;
            RouteDto backwardRoute = null;

            // ----------------------
            // INITIAL valitations
            // ----------------------

            if (this.IsTripGenDataValid(dto))
            {
                using (MdrClientService client = new MdrClientService())
                {
                    var routes = client.GetRoutes(dto.LineId).Result;

                    // ----------------------
                    // SEARCH ROUTES IN DB
                    // ----------------------

                    this.LookUpRoutes(dto, ref singleRoute, ref outwardRoute, ref backwardRoute, routes);

                    if (dto.SingleTrip)
                    {
                        dto.NTrips = 1;
                    }

                    // ----------------------
                    // PREPARE LOOP VARIABLES
                    // ----------------------

                    // this holds all the seconds passed
                    int totalTimePassed = dto.StartTime; 

                    // this holds the seconds of the day (can be converted into time without going past 24h)
                    var tripStartTime = totalTimePassed; 

                    // We need to account for end time lower than start time (it goes past 24h). 
                    var endTime = dto.EndTime < dto.StartTime ? (((SECONDS_IN_DAY - dto.StartTime) + dto.EndTime) + dto.StartTime) : dto.EndTime;

                    // The first trip is always outgoing
                    var tripIdentifierSuffix = 1;

                    // The number of trips will dictate if the stop condition is by end time or by number of trips
                    var byEndTime = dto.NTrips == 0;

                    // Freq in seconds
                    var freq = dto.Frequency * 60;

                    // FIFO. Allows us to get the first trip to come back. Queue<Tuple<A,B>> where 
                    // A = [seconds of the day when the veichle will arive back] 
                    //and 
                    // B = [seconds since the start time when the veichle will arive back]
                    var tripsDispatched = new Queue<Tuple<int,int>>();

                    // The number of veichles currently running trips
                    var veichlesDeployed = 0;

                    // A flag just to indicate if we have to take into account parallel running
                    var isLimitedNumberOfVeichles = dto.MaxVeichlesInParallel > 0;
                    
                    // ----------------------
                    // LOOP
                    // ----------------------
                    
                    do
                    {
                        if (dto.SingleTrip)
                        {
                            var schedules = new List<Schedule>();
                            this.GenerateRouteSchedules(singleRoute, tripStartTime, schedules);
                            trips2Add.Add(new TripDto(Guid.Empty, dto.TripIdentifier, dto.LineId, singleRoute.Id, schedules));
                        }
                        else
                        {
                            // This wll save the time added by these trips taking place. It will be summed to the total
                            var timePassedInThisLoop = 0;

                            // Fetch a start time, if we already deplyed all veichles
                            if (isLimitedNumberOfVeichles && veichlesDeployed >= dto.MaxVeichlesInParallel)
                            {
                                var tuple = tripsDispatched.Dequeue();
                                tripStartTime = tuple.Item1;
                                totalTimePassed = tuple.Item2;
                            }

                            if (byEndTime && this.EndTimeHasBeenReached(endTime, 
                                totalTimePassed 
                                + outwardRoute.RouteSegments.Sum(x => x.Duration)
                                + backwardRoute.RouteSegments.Sum(x => x.Duration)))
                            {
                                // Check if the trip duration (both go and return) doesn't go 
                                // over the end time. If it does, we cannot create any more trips.
                                break;
                            }

                            // GO Trip
                            var schedules = new List<Schedule>();
                            int tripEndTime = this.GenerateRouteSchedules(outwardRoute, tripStartTime, schedules);
                            trips2Add.Add(new TripDto(Guid.Empty, dto.TripIdentifier + "-" + tripIdentifierSuffix++, dto.LineId, outwardRoute.Id, schedules));
                            timePassedInThisLoop += outwardRoute.RouteSegments.Sum(x => x.Duration);

                            // RETURN Trip
                            if (byEndTime || !TripNumberIsMet(dto.NTrips, trips2Add.Count))
                            {
                                schedules = new List<Schedule>();
                                tripEndTime = this.GenerateRouteSchedules(backwardRoute, tripEndTime, schedules);
                                trips2Add.Add(new TripDto(Guid.Empty, dto.TripIdentifier + "-" + tripIdentifierSuffix++, dto.LineId, backwardRoute.Id, schedules));
                                timePassedInThisLoop += backwardRoute.RouteSegments.Sum(x => x.Duration);
                            }

                            // Save the time that this trip returns
                            if (isLimitedNumberOfVeichles)
                            {
                                tripsDispatched.Enqueue(new Tuple<int,int>(tripEndTime, totalTimePassed + timePassedInThisLoop));
                                // Once we have [dto.MaxTripsInParallel] veichles out there, 
                                // we will always have to look up startTimes in the queue
                                veichlesDeployed = Math.Min(veichlesDeployed+1, dto.MaxVeichlesInParallel); 
                            }

                            // Frequency
                            if (freq == 0) 
                            {
                                // The next trip can start immeadiately after the veichle arrives back
                                tripStartTime = tripEndTime;
                                totalTimePassed += timePassedInThisLoop;
                            }
                            else 
                            {
                                // The next trip can start x time (x = freq) after this last outward trip departed
                                tripStartTime += freq;
                                totalTimePassed += freq;
                            }
                        }
                    }
                    while (!this.IsDone(byEndTime, endTime, totalTimePassed, dto.NTrips, trips2Add.Count));

                    // ----------------------
                    // ADD IN DB
                    // ----------------------

                    try
                    {
                        // TODO  Currently missing the following:
                        // "não devem existir viagens que iniciem à mesma hora para uma dada linha/percurso. podem existir viagens de linhas diferentes num mesmo nó à mesma hora" 
                        foreach (var trip in trips2Add)
                        {
                            var addedTrip = await this._repo.AddAsync(new Trip(trip.TripIdentifier, trip.LineId, trip.RouteId, trip.Schedule));
                            trip.Id = addedTrip.Id.AsGuid();
                        }

                        await this._unitOfWork.CommitAsync();
                    }
                    catch (Exception e)
                    {
                        throw new BusinessRuleValidationException(e.Message);
                    } 
                }
            }
            return trips2Add;
        }

        private void LookUpRoutes(TripGenerationDto dto, ref RouteDto singleRoute, ref RouteDto outwardRoute, ref RouteDto backwardRoute, List<RouteDto> routes)
        {
            if (dto.SingleTrip)
            {
                // Search route
                if (routes.FirstOrDefault(x => x.Id == dto.Route) is RouteDto x)
                {
                    singleRoute = x;
                }
                else throw new BusinessRuleValidationException("Could not find a Route with id [" + dto.Route + "] in the database.");
            }
            else
            {
                // Search out route
                if (routes.FirstOrDefault(x => x.Id == dto.RouteOutward) is RouteDto x)
                {
                    outwardRoute = x;
                }
                else throw new BusinessRuleValidationException("Could not find a Route with id [" + dto.Route + "] in the database.");

                // Search back route
                if (routes.FirstOrDefault(x => x.Id == dto.RouteBackward) is RouteDto y)
                {
                    backwardRoute = y;
                }
                else throw new BusinessRuleValidationException("Could not find a Route with id [" + dto.Route + "] in the database.");
            }
        }

        private int GenerateRouteSchedules(RouteDto singleRoute, int currentTripStartTime, List<Schedule> schedules)
        {
            int nodeDepartTime = currentTripStartTime;
            for (int i = 0; i < singleRoute.RouteSegments.Count; i++)
            {
                var segment = singleRoute.RouteSegments[i];
                if (i == 0)
                {
                    schedules.Add(new Schedule()
                    {
                        NodeId = segment.StartNode,
                        TimeInSeconds = nodeDepartTime,
                    });
                }

                nodeDepartTime = this.IncrementTimeForHourConversion(nodeDepartTime, segment.Duration);

                schedules.Add(new Schedule()
                {
                    NodeId = segment.EndNode,
                    TimeInSeconds = nodeDepartTime
                });
            }

            return nodeDepartTime;
        }

        /// <summary>
        /// Reset Time if over midnight
        /// </summary>
        /// <param name="currentTime"></param>
        /// <param name="timeToAdd"></param>
        /// <returns></returns>
        int IncrementTimeForHourConversion(int currentTime, int timeToAdd) 
        {
            var addedTime = timeToAdd + currentTime;
            return addedTime > SECONDS_IN_DAY ? addedTime - SECONDS_IN_DAY : addedTime;
        }

        bool IsDone(bool byEndTime, int endTime, int currentTime, int totalTrips, int currentTrips)
        {
            return byEndTime ? 
                EndTimeHasBeenReached(endTime, currentTime) : 
                TripNumberIsMet(totalTrips, currentTrips);
        }

        bool EndTimeHasBeenReached(int endTime, int currentTime)
        {
            return currentTime >= endTime;
        }

        bool TripNumberIsMet(int totalTrips, int currentTrips) {
            return totalTrips == currentTrips;
        }

        private bool IsTripGenDataValid(TripGenerationDto dto)
        {
            if (string.IsNullOrEmpty(dto.LineId))
            {
                throw new BusinessRuleValidationException("LineId cannot be empty or null.");
            }
            else if (dto.SingleTrip)
            {
                if (string.IsNullOrEmpty(dto.Route))
                {
                    throw new BusinessRuleValidationException("For single trip generation a route must be specified.");
                }
            }
            else
            {
                if (string.IsNullOrEmpty(dto.RouteOutward) || string.IsNullOrEmpty(dto.RouteBackward))
                {
                    throw new BusinessRuleValidationException("For multiple trip generation both the outward and backward route must be specified.");
                }
                else if (dto.EndTime == 0 && dto.NTrips == 0)
                {
                    throw new BusinessRuleValidationException("For multiple trip generation, the number of trips or an end time must be provided must be provided.");
                }
            }

            return true;
        }

        //TODO
        public async Task<TripDto> DeleteAsync(TripId id)
        {
            var trip = await this._repo.GetByIdAsync(id);

            if (trip == null)
                return null;

            this._repo.Remove(trip);
            await this._unitOfWork.CommitAsync();

            return new TripDto(trip.Id.AsGuid(), trip.TripIdentifier, trip.LineId, trip.RouteId, trip.Schedules);
        }
    }
}
﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Trips;
using DDDSample1.Services;

namespace DDDSample1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TripsController : ControllerBase
    {
        private readonly TripService _service;

        public TripsController(TripService service)
        {
            _service = service;
        }

        //// GET: api/trips
        /// <summary>
        /// My get controller
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TripDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }

        // GET: api/trips/5
        [HttpGet("{id}")]
        public async Task<ActionResult<TripDto>> GetGetById(Guid id)
        {
            var trip = await _service.GetByIdAsync(new TripId(id));

            if (trip == null)
            {
                return NotFound();
            }
            return trip;
        }

        //// POST: api/trips
        [HttpPost]
        public async Task<ActionResult<TripDto>> Create(CreatingTripDto dto)
        {
            try
            {
                var trip = await _service.AddAsync(dto);
                return CreatedAtAction(nameof(GetGetById), new { id = trip.Id }, trip);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }

        //// POST: api/gen
        [HttpPost("gen")]
        public async Task<ActionResult<List<TripDto>>> Generate(TripGenerationDto dto)
        {
            try
            {
                var trip = await _service.GenerateTrips(dto);
                return Ok(trip);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }


        //// PUT: api/trips
        [HttpPut()]
        public async Task<ActionResult<TripDto>> Update(TripDto dto)
        {
            if (dto == null || string.IsNullOrEmpty(dto.Id.ToString()))
            {
                return BadRequest();
            }
            try
            {
                var trip = await _service.UpdateAsync(dto);
                if (trip == null)
                {
                    return NotFound();
                }
                return Ok(trip);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }

        //// DELETE: api/trips/5
        [HttpDelete("{id}/hard")]
        public async Task<ActionResult<TripDto>> HardDelete(Guid id)
        {
            try
            {
                var trip = await _service.DeleteAsync(new TripId(id));
                if (trip == null)
                {
                    return NotFound();
                }
                return Ok(trip);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }
    }
}
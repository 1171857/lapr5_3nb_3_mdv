﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Vehicles;


namespace DDDSample1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehiclesController : ControllerBase
    {
        private readonly VehicleService _service;

        public VehiclesController(VehicleService service)
        {
            _service = service;
        }

        // GET: api/Vehicles
        [HttpGet]
        public async Task<ActionResult<IEnumerable<VehicleDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }

        // GET: api/Vehicles/5
        [HttpGet("{id}")]
        public async Task<ActionResult<VehicleDto>> GetGetById(Guid id)
        {
            var vehicle = await _service.GetByIdAsync(new VehicleId(id));

            if (vehicle == null)
            {
                return NotFound();
            }
            return vehicle;
        }

        // POST: api/Vehicles
        [HttpPost]
        public async Task<ActionResult<VehicleDto>> Create(CreatingVehicleDto dto)
        {
            try
            {
                var vehicle = await _service.AddAsync(dto);
                return CreatedAtAction(nameof(GetGetById), new { id = vehicle.Id }, vehicle);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }


        // PUT: api/Vehicles/5
        [HttpPut("{id}")]
        public async Task<ActionResult<VehicleDto>> Update(Guid id, VehicleDto dto)
        {
            if (id != dto.Id)
            {
                return BadRequest();
            }
            try
            {
                var vehicle = await _service.UpdateAsync(dto);
                if (vehicle == null)
                {
                    return NotFound();
                }
                return Ok(vehicle);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }

        // DELETE: api/Vehicles/5
        [HttpDelete("{id}/hard")]
        public async Task<ActionResult<VehicleDto>> HardDelete(Guid id)
        {
            try
            {
                var vehicle = await _service.DeleteAsync(new VehicleId(id));
                if (vehicle == null)
                {
                    return NotFound();
                }
                return Ok(vehicle);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }
    }
}
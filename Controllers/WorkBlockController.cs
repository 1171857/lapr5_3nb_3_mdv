using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.WorkBlocks;

namespace DDDSample1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WorkBlocksController : ControllerBase
    {
        private readonly WorkBlockService _service;

        public WorkBlocksController(WorkBlockService service)
        {
            _service = service;
        }

        // GET: api/WorkBlocks
        [HttpGet]
        public async Task<ActionResult<IEnumerable<WorkBlockDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }

        // GET: api/Workblock/assignable
        [HttpGet("assignable")]
        public async Task<ActionResult<IEnumerable<WorkBlockDto>>> GetAllAssignable()
        {
            return await _service.GetAssignableAsync();
        }

        // GET: api/Workblock/5
        [HttpGet("{id}")]
        public async Task<ActionResult<WorkBlockDto>> GetGetById(Guid id)
        {
            var workBlock = await _service.GetByIdAsync(new WorkBlockId(id));

            if (workBlock == null)
            {
                return NotFound();
            }

            return workBlock;
        }

        // POST: api/Workblock
        [HttpPost]
        public async Task<ActionResult<WorkBlockDto>> Create(CreatingWorkBlockDto dto)
        {
            var workBlock = await _service.AddAsync(dto);

            return CreatedAtAction(nameof(GetGetById), new { id = workBlock.Id, startTime = workBlock.StartTime, 
                endTime = workBlock.EndTime, listTrips = workBlock.ListTrips }, workBlock);
        }


        // PUT: api/Workblock/5
        [HttpPut("{id}")]
        public async Task<ActionResult<WorkBlockDto>> Update(Guid id, WorkBlockDto dto)
        {
            if (id != dto.Id)
            {
                return BadRequest();
            }

            try
            {
                var cat = await _service.UpdateAsync(dto);

                if (cat == null)
                {
                    return NotFound();
                }
                return Ok(cat);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }

        // DELETE: api/Workblock/5
        [HttpDelete("{id}/hard")]
        public async Task<ActionResult<WorkBlockDto>> HardDelete(Guid id)
        {
            try
            {
                var cat = await _service.DeleteAsync(new WorkBlockId(id));

                if (cat == null)
                {
                    return NotFound();
                }

                return Ok(cat);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }
    }
}
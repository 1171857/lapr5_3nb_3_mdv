﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Drivers;
using DDDSample1.Dtos.Drivers;
using DDDSample1.Services;


namespace DDDSample1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DriversController : ControllerBase
    {
        private readonly DriverService _service;

        public DriversController(DriverService service)
        {
            _service = service;
        }

        // GET: api/drivers
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DriverDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }

        // GET: api/drivers/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DriverDto>> GetGetById(Guid id)
        {
            var driver = await _service.GetByIdAsync(new DriverId(id));

            if (driver == null)
            {
                return NotFound();
            }
            return driver;
        }

        // POST: api/drivers
        [HttpPost]
        public async Task<ActionResult<DriverDto>> Create(CreatingDriverDto dto)
        {
            try
            {
                var driver = await _service.AddAsync(dto);
                return CreatedAtAction(nameof(GetGetById), new { id = driver.Id }, driver);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }


        // PUT: api/drivers/5
        [HttpPut("{id}")]
        public async Task<ActionResult<DriverDto>> Update(Guid id, DriverDto dto)
        {
            if (id != dto.Id)
            {
                return BadRequest();
            }
            try
            {
                var driver = await _service.UpdateAsync(dto);
                if (driver == null)
                {
                    return NotFound();
                }
                return Ok(driver);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }

        // DELETE: api/drivers/5
        [HttpDelete("{id}/hard")]
        public async Task<ActionResult<DriverDto>> HardDelete(Guid id)
        {
            try
            {
                var driver = await _service.DeleteAsync(new DriverId(id));
                if (driver == null)
                {
                    return NotFound();
                }
                return Ok(driver);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }
    }
}
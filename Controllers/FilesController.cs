﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using DDDSample1.Services;
using Microsoft.AspNetCore.Http;
using System.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using DDDSample1.Domain.Trips;
using DDDSample1.Domain.WorkBlocks;
using DDDSample1.Domain.VehicleServices;
using DDDSample1.Domain.DriverServices;

namespace DDDSample1.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [Route("api/[controller]")]
    [ApiController]
    public class FilesController : ControllerBase
    {
        private readonly TripService _tripService;
        private readonly WorkBlockService _workBlockService;
        private readonly VehicleServiceService _vehicleServiceService;
        private readonly DriverServiceService _driverServiceService;

        public FilesController(TripService tripService, WorkBlockService workBlockService, VehicleServiceService vehicleServiceService, DriverServiceService driverServiceService)
        {
            _tripService = tripService;
            _workBlockService = workBlockService;
            _vehicleServiceService = vehicleServiceService;
            _driverServiceService = driverServiceService;
        }

        // POST: api/files
        [HttpPost]
        public async Task<IActionResult> Create([FromForm(Name = "file")] IFormFile xmlFile)
        {
            try
            {
                var xml = new XmlDocument();
                xml.Load(xmlFile.OpenReadStream());
                string jsonText = JsonConvert.SerializeXmlNode(xml, Newtonsoft.Json.Formatting.Indented);
                string jsonString = Regex.Replace(jsonText, "(?<=\")(@)(?!.*\":\\s )", string.Empty, RegexOptions.IgnoreCase);
                dynamic stuff = JObject.Parse(jsonString);
                await ImportTrips(stuff);
                dynamic workblocks = stuff.GlDocumentInfo.world.GlDocument.GlDocumentSchedule.Schedule.WorkBlocks;
                await ImportWorkblocks(workblocks);
                dynamic vehicleServices = stuff.GlDocumentInfo.world.GlDocument.GlDocumentSchedule.Schedule.VehicleDuties;
                await ImportVehicleServices(vehicleServices);
                dynamic driverServices = stuff.GlDocumentInfo.world.GlDocument.GlDocumentSchedule.Schedule.DriverDuties;
                await ImportDriverServices(driverServices);
                return Ok();
            }
            catch (Exception exception)
            {
                return BadRequest($"Error: {exception.Message}");
            }
        }

        public async Task<Boolean> ImportTrips(dynamic data)
        {
            dynamic trips = data.GlDocumentInfo.world.GlDocument.GlDocumentSchedule.Schedule.Trips;
            dynamic nodesList = data.GlDocumentInfo.world.GlDocument.GlDocumentNetwork.Network.Nodes.Node;
            int tripsCount = trips.Trip.Count;
            for (int i = 0; i < tripsCount; i++)
            {
                List<CreatingScheduleDto> schedule = new List<CreatingScheduleDto>();
                dynamic passingTimes = trips.Trip[i].PassingTimes.PassingTime;
                int passingTimeCount = passingTimes.Count;
                for (int j = 0; j < passingTimeCount; j++)
                {
                    String nodeKey = passingTimes[j].Node;
                    String node = getNodeNameByKey(nodesList, nodeKey);
                    int time = passingTimes[j].Time;
                    CreatingScheduleDto scheduleDto = new CreatingScheduleDto(node, time);
                    schedule.Add(scheduleDto);
                }
                string id = trips.Trip[i].key;
                string orientation = trips.Trip[i].Orientation;
                string line;
                try
                {
                    line = trips.Trip[i].Line;
                }
                catch
                {
                    line = "";
                }
                string route = trips.Trip[i].Path;
                CreatingTripDto tripDto = new CreatingTripDto(id, orientation, line, route, schedule);
                var trip = await _tripService.AddAsync(tripDto);
            }
            return true;
        }

        private String getNodeNameByKey(dynamic nodesList, String searchValue)
        {
            String ret = "";

            for (int i = 0; i < nodesList.Count; i++)
            {
                String key = nodesList[i].key;
                if (key == searchValue)
                {
                    return nodesList[i].ShortName;
                }
            }

            return ret;
        }

        public async Task<Boolean> ImportWorkblocks(dynamic workblocks)
        {
            int workblocksCount = workblocks.WorkBlock.Count;
            for (int i = 0; i < workblocksCount; i++)
            {

                List<TripId> trips = new List<TripId>();
                dynamic tripsObj = null;
                Boolean hasTrips = true;
                try
                {
                    tripsObj = workblocks.WorkBlock[i].Trips.refe;
                }
                catch
                {
                    hasTrips = false;
                }
                if (hasTrips)
                {
                    int tripsCount;
                    try
                    {
                        tripsCount = tripsObj.Count;
                    }
                    catch
                    {
                        tripsCount = 1;
                    }
                    if (tripsCount > 1)
                    {
                        for (int j = 0; j < tripsCount; j++)
                        {
                            string tripIdentifier = tripsObj[j].key;
                            TripId tripid = new TripId((await _tripService.GetByIdentifierAsync(tripIdentifier)).Id.ToString());
                            trips.Add(tripid);
                        }
                    }
                    else
                    {
                        string tripIdentifier = tripsObj.key;
                        TripId tripid = new TripId((await _tripService.GetByIdentifierAsync(tripIdentifier)).Id.ToString());
                        trips.Add(tripid);
                    }
                }
                string id = workblocks.WorkBlock[i].key;
                int startTime = workblocks.WorkBlock[i].StartTime;
                int endTime = workblocks.WorkBlock[i].EndTime;
                if (endTime > 86400)
                {
                    endTime = 86400;
                }
                CreatingWorkBlockDto wbDto = new CreatingWorkBlockDto();
                wbDto.WorkBlockIdentifier = id;
                wbDto.StartTime = startTime;
                wbDto.EndTime = endTime;
                wbDto.ListTrips = trips;
                var wb = await _workBlockService.AddAsync(wbDto);
            }
            return true;
        }

        public async Task<Boolean> ImportVehicleServices(dynamic vehicleServices)
        {
            int vehicleServicesCount = vehicleServices.VehicleDuty.Count;
            for (int i = 0; i < vehicleServicesCount; i++)
            {
                List<WorkBlockId> wbIds = new List<WorkBlockId>();
                dynamic wbObj = null;
                int startTime = 85000;
                int endTime = -1;
                try
                {
                    wbObj = vehicleServices.VehicleDuty[i].WorkBlocks.refe;
                }
                catch
                {
                    continue;
                }
                int wbsCount;
                try
                {
                    wbsCount = wbObj.Count;
                }
                catch
                {
                    wbsCount = 1;
                }
                if (wbsCount > 1)
                {
                    for (int j = 0; j < wbsCount; j++)
                    {
                        string wbIdentifier = wbObj[j].key;
                        WorkBlockDto wbDto = await _workBlockService.GetByIdentifierAsync(wbIdentifier);
                        WorkBlockId wbId = new WorkBlockId(wbDto.Id.ToString());
                        wbIds.Add(wbId);
                        if (wbDto.StartTime < startTime)
                        {
                            startTime = wbDto.StartTime;
                        }
                        if (wbDto.EndTime > endTime)
                        {
                            endTime = wbDto.EndTime;
                        }
                    }
                }
                else
                {
                    string wbIdentifier = wbObj.key;
                    WorkBlockDto wbDto = await _workBlockService.GetByIdentifierAsync(wbIdentifier);
                    WorkBlockId wbId = new WorkBlockId(wbDto.Id.ToString());
                    wbIds.Add(wbId);
                    if (wbDto.StartTime < startTime)
                    {
                        startTime = wbDto.StartTime;
                    }
                    if (wbDto.EndTime > endTime)
                    {
                        endTime = wbDto.EndTime;
                    }
                }
                string id = vehicleServices.VehicleDuty[i].key;
                CreatingVehicleServiceDto vehicleServiceDto = new CreatingVehicleServiceDto();
                vehicleServiceDto.VehicleServiceIdentifier = id;
                vehicleServiceDto.StartTime = startTime;
                vehicleServiceDto.EndTime = endTime;
                vehicleServiceDto.ListWorkBlocks = wbIds;
                var vehicleService = await _vehicleServiceService.AddAsync(vehicleServiceDto);
            }
            return true;
        }

        public async Task<Boolean> ImportDriverServices(dynamic driverServices)
        {
            int driverServicesCount = driverServices.DriverDuty.Count;
            for (int i = 0; i < driverServicesCount; i++)
            {
                List<WorkBlockId> wbIds = new List<WorkBlockId>();
                dynamic wbObj = null;
                int startTime = 85000;
                int endTime = -1;
                try
                {
                    wbObj = driverServices.DriverDuty[i].WorkBlocks.refe;
                }
                catch
                {
                    continue;
                }
                int wbsCount;
                try
                {
                    wbsCount = wbObj.Count;
                }
                catch
                {
                    wbsCount = 1;
                }
                if (wbsCount > 1)
                {
                    for (int j = 0; j < wbsCount; j++)
                    {
                        string wbIdentifier = wbObj[j].key;
                        WorkBlockDto wbDto = await _workBlockService.GetByIdentifierAsync(wbIdentifier);
                        WorkBlockId wbId = new WorkBlockId(wbDto.Id.ToString());
                        wbIds.Add(wbId);
                        if (wbDto.StartTime < startTime)
                        {
                            startTime = wbDto.StartTime;
                        }
                        if (wbDto.EndTime > endTime)
                        {
                            endTime = wbDto.EndTime;
                        }
                    }
                }
                else
                {
                    string wbIdentifier = wbObj.key;
                    WorkBlockDto wbDto = await _workBlockService.GetByIdentifierAsync(wbIdentifier);
                    WorkBlockId wbId = new WorkBlockId(wbDto.Id.ToString());
                    wbIds.Add(wbId);
                    if (wbDto.StartTime < startTime)
                    {
                        startTime = wbDto.StartTime;
                    }
                    if (wbDto.EndTime > endTime)
                    {
                        endTime = wbDto.EndTime;
                    }
                }
                string id = driverServices.DriverDuty[i].key;
                CreatingDriverServiceDto driverServiceDto = new CreatingDriverServiceDto();
                driverServiceDto.DriverServiceIdentifier = id;
                driverServiceDto.StartTime = startTime;
                driverServiceDto.EndTime = endTime;
                driverServiceDto.ListWorkBlocks = wbIds;
                var driverService = await _driverServiceService.AddAsync(driverServiceDto);
            }
            return true;
        }
    }
}
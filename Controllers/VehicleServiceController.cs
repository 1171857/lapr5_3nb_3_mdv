﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.VehicleServices;

namespace DDDSample1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VehicleServicesController : ControllerBase
    {
        private readonly VehicleServiceService _service;

        public VehicleServicesController(VehicleServiceService service)
        {
            _service = service;
        }

        // GET: api/vehicle-services
        [HttpGet]
        public async Task<ActionResult<IEnumerable<VehicleServiceDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }

        // GET: api/vehicle-services/5
        [HttpGet("{id}")]
        public async Task<ActionResult<VehicleServiceDto>> GetGetById(Guid id)
        {
            var vehicleService = await _service.GetByIdAsync(new VehicleServiceId(id));

            if (vehicleService == null)
            {
                return NotFound();
            }

            return vehicleService;
        }

        // POST: api/vehicle-services
        [HttpPost]
        public async Task<ActionResult<VehicleServiceDto>> Create(CreatingVehicleServiceDto dto)
        {
            var vehicleService = await _service.AddAsync(dto);

            return CreatedAtAction(nameof(GetGetById), new
            {
                id = vehicleService.Id,
                startTime = vehicleService.StartTime,
                endTime = vehicleService.EndTime,
                listTrips = vehicleService.ListTrips
            }, vehicleService);
        }


        // PUT: api/vehicle-services/5
        [HttpPut("{id}")]
        public async Task<ActionResult<VehicleServiceDto>> Update(Guid id, VehicleServiceDto dto)
        {
            if (id != dto.Id)
            {
                return BadRequest();
            }

            try
            {
                var cat = await _service.UpdateAsync(dto);

                if (cat == null)
                {
                    return NotFound();
                }
                return Ok(cat);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }

        // DELETE: api/Workblock/5
        [HttpDelete("{id}/hard")]
        public async Task<ActionResult<VehicleServiceDto>> HardDelete(Guid id)
        {
            try
            {
                var cat = await _service.DeleteAsync(new VehicleServiceId(id));

                if (cat == null)
                {
                    return NotFound();
                }

                return Ok(cat);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }
    }
}
﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.DriverServices;

namespace DDDSample1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DriverServicesController : ControllerBase
    {
        private readonly DriverServiceService _service;

        public DriverServicesController(DriverServiceService service)
        {
            _service = service;
        }

        //// GET: api/DriverServices
        [HttpGet]
        public async Task<ActionResult<IEnumerable<DriverServiceDto>>> GetAll()
        {
            return await _service.GetAllAsync();
        }

        // GET: api/DriverServices/5
        [HttpGet("{id}")]
        public async Task<ActionResult<DriverServiceDto>> GetGetById(Guid id)
        {
            var DriverService = await _service.GetByIdAsync(new DriverServiceId(id));

            if (DriverService == null)
            {
                return NotFound();
            }
            return DriverService;
        }

        //// POST: api/DriverServices
        [HttpPost]
        public async Task<ActionResult<DriverServiceDto>> Create(CreatingDriverServiceDto dto)
        {
            var driverService = await _service.AddAsync(dto);

            return CreatedAtAction(nameof(GetGetById), new
            {
                id = driverService.Id,
                startTime = driverService.StartTime,
                endTime = driverService.EndTime,
                listWorkblocks = driverService.ListWorkblocks
            }, driverService);
        }
     
        //// PUT: api/DriverServices
        [HttpPut()]
        public async Task<ActionResult<DriverServiceDto>> Update(DriverServiceDto dto)
        {
            if (dto == null || string.IsNullOrEmpty(dto.Id.ToString()))
            {
                return BadRequest();
            }
            try
            {
                var DriverService = await _service.UpdateAsync(dto);
                if (DriverService == null)
                {
                    return NotFound();
                }
                return Ok(DriverService);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }

        //// DELETE: api/DriverServices/5
        [HttpDelete("{id}/hard")]
        public async Task<ActionResult<DriverServiceDto>> HardDelete(Guid id)
        {
            try
            {
                var DriverService = await _service.DeleteAsync(new DriverServiceId(id));
                if (DriverService == null)
                {
                    return NotFound();
                }
                return Ok(DriverService);
            }
            catch (BusinessRuleValidationException ex)
            {
                return BadRequest(new { Message = ex.Message });
            }
        }
    }
}
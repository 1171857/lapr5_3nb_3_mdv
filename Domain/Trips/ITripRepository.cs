using System;
using System.Threading.Tasks;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.Trips
{
    public interface ITripRepository: IRepository<Trip,TripId>
    {
        public Task<Trip> GetByIdentifierAsync(String identifier);
    }
}
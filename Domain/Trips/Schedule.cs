using DDDSample1.Domain.Shared;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace DDDSample1.Domain.Trips
{
    public class Schedule : IValueObject
    {
        [JsonProperty(PropertyName = "NodeId")]
        public string NodeId { get; set; }
        [JsonProperty(PropertyName = "TimeInSeconds")]
        public int TimeInSeconds { get; set; }
        
        public Schedule()
        {

        }
        public Schedule(string nodeId, string timeInSeconds)
        {
            this.NodeId = nodeId;
            this.TimeInSeconds = Int32.Parse(timeInSeconds);
        }

        public Schedule(string nodeId, int timeInSeconds)
        {
            this.NodeId = nodeId;
            this.TimeInSeconds = timeInSeconds;
        }
        public Schedule(string information)
        {
            string[] info = information.Split(":");
            this.NodeId = info[0];
            try
            {
                this.TimeInSeconds = Int32.Parse(info[1]);
            }
            catch (FormatException)
            {
                Console.WriteLine($"Unable to parse '{info[1]}'");
            }

        }

        protected IEnumerable<object> GetEqualityComponents()
        {
            // Using a yield return statement to return each element one at a time
            yield return NodeId;
            yield return TimeInSeconds;
        }

        public override string ToString()
        {
            return this.NodeId + ":" + this.TimeInSeconds.ToString();
        }
    }
}
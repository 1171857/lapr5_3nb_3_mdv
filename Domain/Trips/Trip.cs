using System;
using System.Collections.Generic;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.WorkBlocks;
using DDDSample1.Domain.VehicleServices;

namespace DDDSample1.Domain.Trips
{
    public class Trip : Entity<TripId>, IAggregateRoot
    {
        public string TripIdentifier { get;  private set; }

        public string LineId { get; private set; }

        public string RouteId { get; private set; }

        public List<Schedule> Schedules { get; private set; }

        //public List<string> Nodes { get; private set; }

        //public bool IsActive { get; private set; }

        public ICollection<WorkBlock> WorkBlocks { get; set; }
        public ICollection<VehicleService> VehicleServices { get; set; }

        private Trip()
        {

        }

        public Trip(string tripIdentifier, string lineId, string routeId, List<Schedule> schedule)
        {
            this.Id = new TripId(Guid.NewGuid());
            this.TripIdentifier = tripIdentifier;
            this.LineId = lineId;
            this.RouteId = routeId;
            this.Schedules = schedule;
            //this.Nodes = nodes;
        }

        public void ChangeTripIdentifier(string tripIdentifier)
        {
            //if (!this.IsActive)
            //    throw new BusinessRuleValidationException("It is not possible to change the description to an inactive product.");
            this.TripIdentifier = tripIdentifier;
        }

        public void ChangeRouteId(string routeId)
        {
            //if (!this.IsActive)
            //    throw new BusinessRuleValidationException("It is not possible to change the description to an inactive product.");
            this.RouteId = routeId;
        }

        public void ChangeSchedules(List<Schedule> schedules)
        {

            //if (!this.IsActive)
            //    throw new BusinessRuleValidationException("It is not possible to change the description to an inactive product.");
            this.Schedules = schedules;
        }

        public void ChangeWorkBlocks(ICollection<WorkBlock> workBlocks)
        {
            //if (!this.IsActive)
            //    throw new BusinessRuleValidationException("It is not possible to change the description to an inactive product.");
            this.WorkBlocks = workBlocks;
        }

        private List<Schedule> createListOfSchedules(List<string> schedules)
        {
            List<Schedule> newList = new List<Schedule>();
            foreach(string element in schedules)
            {
                newList.Add(new Schedule(element));
            }
            return newList;
        }
    }
}
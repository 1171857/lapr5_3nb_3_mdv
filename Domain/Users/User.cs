using System;
using System.Text.RegularExpressions;
using DDDSample1.Domain.Shared;
namespace DDDSample1.Domain.Users
{
    public class User : Entity<UserId>, IAggregateRoot
    {

        private readonly Regex regexEmail = new Regex(@"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$");

        private readonly int MIN_LENGTH = 4;
        public string Name { get; private set; }
        public string Email { get; private set; }
        public string Password { get; private set; }
        public DateTime AcceptanceOfPrivacyPolicy { get; private set; }
        public DateTime LastLogin { get; private set; }
        public Role Role { get; private set; }

        private User()
        {

        }

        public User(string name, string email, string password, Role role)
        {
            if (!regexEmail.IsMatch(email)){
                throw new BusinessRuleValidationException("Invalid email!");
            }
            if(email == null || email.Length == 0)
            {
                throw new BusinessRuleValidationException("Invalid email can't be null or empty!");
            }
            if (password == null || password.Length <= MIN_LENGTH)
            {
                throw new BusinessRuleValidationException("Invalid password can't be null or less then " + MIN_LENGTH + " digits!");
            }
            this.Id = new UserId(Guid.NewGuid());
            this.Name = name;
            this.Email = email;
            this.Password = BCrypt.Net.BCrypt.HashPassword(password);
            this.AcceptanceOfPrivacyPolicy = DateTime.Now;
            this.LastLogin = DateTime.Now;
            this.Role = role;
        }
    }
}
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text.RegularExpressions;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Trips;
using DDDSample1.Domain.VehicleServices;

namespace DDDSample1.Domain.WorkBlocks
{
    public class WorkBlock : Entity<WorkBlockId>, IAggregateRoot
    {

        private readonly int WORK_BLOCK_HOUR_LIMIT = 14400;
        private readonly int MAX_HOUR_IN_SEC = 86400;
        private readonly int MIN_HOUR_IN_SEC = 0;
        private readonly Regex regex = new Regex("^[a-zA-Z0-9]*$");

        public string WorkBlockIdentifier { get; private set; }

        public int StartTime { get; private set; }

        public int EndTime { get; private set; }
        
        public int WorkBlockHourLimit { get; private set; }

        public ICollection<Trip> Trips { get; private set; }

        public VehicleService VehicleService { get; private set; }
                
        private WorkBlock()
        {
            
        }

        public WorkBlock(string workBlockIdentifier, int startTime, int endTime, ICollection<Trip> trips, VehicleService vehicleService)
        {
            if(startTime> MAX_HOUR_IN_SEC || startTime < MIN_HOUR_IN_SEC ||
                endTime > MAX_HOUR_IN_SEC || endTime < MIN_HOUR_IN_SEC)
            {
                throw new BusinessRuleValidationException("Invalid start or end time. Must be between 0 and 86400 seconds!");
            }

            //if (!regex.IsMatch(workBlockIdentifier))
            //{
            //    throw new BusinessRuleValidationException("Id must be alphanumeric!");
            //}
            if (workBlockIdentifier == null || workBlockIdentifier.Length == 0 )
            {
                throw new BusinessRuleValidationException("Every workblock requires a identifier.");
            }
            if(startTime >= endTime)
            {
                throw new BusinessRuleValidationException("The start time must be before the end time!");
            }
            this.Id = new WorkBlockId(Guid.NewGuid());
            this.WorkBlockIdentifier = workBlockIdentifier;
            this.StartTime = startTime;
            this.EndTime = endTime;
            this.WorkBlockHourLimit = 14400;
            this.Trips = trips;
            this.VehicleService = vehicleService;
        }

        public void ChangeWorkBlockHourLimite(int newWorkBlockHourLimit)
        {
            if(newWorkBlockHourLimit > MIN_HOUR_IN_SEC && 
                newWorkBlockHourLimit < MAX_HOUR_IN_SEC)
            {
                this.WorkBlockHourLimit = newWorkBlockHourLimit;
            } else
            {
                throw new BusinessRuleValidationException("Invalid work block hour limite. Must be between 0 and 86400 seconds!");
            }
        }

        public void ChangeStartTime(int startTime)
        {
            if (startTime > MIN_HOUR_IN_SEC &&
                startTime < MAX_HOUR_IN_SEC)
            {
                this.StartTime = startTime;
            }
            else
            {
                throw new BusinessRuleValidationException("Invalid Start Time. Must be between 0 and 86400 seconds!");
            }
        }

        public void ChangeEndTime(int endTime)
        {
            if (endTime > MIN_HOUR_IN_SEC &&
                endTime < MAX_HOUR_IN_SEC)
            {
                this.EndTime = endTime;
            }
            else
            {
                throw new BusinessRuleValidationException("Invalid End Time. Must be between 0 and 86400 seconds!");
            }
        }

        public void ChangeListTrips(ICollection<Trip> Trips)
        {
            if (Trips != null && Trips.Any())
            {
                this.Trips =Trips;
            }
            else
            {
                throw new BusinessRuleValidationException("Invalid List. Can't be null or empty");
            }
        }
    }
}
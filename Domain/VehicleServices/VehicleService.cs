﻿using System;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.WorkBlocks;
using DDDSample1.Domain.Trips;
using System.Collections.Generic;

namespace DDDSample1.Domain.VehicleServices
{
    public class VehicleService : Entity<VehicleServiceId>, IAggregateRoot
    {
        private readonly int MAX_HOUR_IN_SEC = 86400;
        private readonly int MIN_HOUR_IN_SEC = 0;

        public string VehicleServiceIdentifier { get; private set; }
        public int StartTime { get; private set; }
        public int EndTime { get; private set; }
        public ICollection<WorkBlock> WorkBlocks { get; set; }
        public ICollection<Trip> Trips { get; private set; }

        private VehicleService()
        {

        }

        public VehicleService(string vehicleServiceIdentifier, int startTime, int endTime, ICollection<Trip> trips)
        {
            this.Id = new VehicleServiceId(Guid.NewGuid());
            if (startTime > MAX_HOUR_IN_SEC || startTime < MIN_HOUR_IN_SEC ||
                endTime > MAX_HOUR_IN_SEC || endTime < MIN_HOUR_IN_SEC)
            {
                throw new BusinessRuleValidationException("Invalid start or end time. Must be between 0 and 86400 seconds!");
            }
            this.VehicleServiceIdentifier = vehicleServiceIdentifier;
            this.StartTime = startTime;
            this.EndTime = endTime;
            this.Trips = trips;
        }

        public void AddWorkBlocks(ICollection<WorkBlock> workblocks)
        {
            this.WorkBlocks = workblocks;
        }
    }
}
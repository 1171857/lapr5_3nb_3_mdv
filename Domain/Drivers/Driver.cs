﻿using System;
using System.Collections.Generic;
using DDDSample1.Domain.Shared;

namespace DDDSample1.Domain.Drivers
{
    public class Driver : Entity<DriverId>, IAggregateRoot
    {
        public string MechanographicalNumber { get; private set; }
        public string Name { get; private set; }
        public DateTime BirthDate { get; private set; }
        public string Nif { get; private set; }
        public string LicenseNumber { get; private set; }
        public DateTime LicenseExpirationDate { get; private set; }
        public DateTime StartDate { get; private set; }
        public DateTime ExitDate { get; private set; }
        public List<string> DriverTypes { get; private set; }

        private Driver()
        {

        }

        public Driver(string mechanographicalNumber, string name,
            DateTime birthDate, string nif, string licenseNumber,
            DateTime licenseExpirationDate, DateTime startDate,
            DateTime exitDate, List<string> driverTypes)
        {
            if (mechanographicalNumber == null || mechanographicalNumber.Length == 0)
                throw new BusinessRuleValidationException("Every driver requires a mechanographical number.");
            if (mechanographicalNumber.Length != 9)
                throw new BusinessRuleValidationException("Mechanographical number requires 9 characters.");
            if (name == null || name.Length == 0)
                throw new BusinessRuleValidationException("Every driver requires a name.");
            if (nif == null || nif.Length == 0)
                throw new BusinessRuleValidationException("Every driver requires a NIF.");
            if (licenseNumber == null || licenseNumber.Length == 0)
                throw new BusinessRuleValidationException("Every driver requires a license number.");

            this.Id = new DriverId(Guid.NewGuid());
            this.MechanographicalNumber = mechanographicalNumber;
            this.Name = name;
            this.BirthDate = birthDate;
            this.Nif = nif;
            this.LicenseNumber = licenseNumber; 
            this.LicenseExpirationDate = licenseExpirationDate;
            this.StartDate = startDate;
            this.ExitDate = exitDate;
            this.DriverTypes = driverTypes;
        }
    }
}
﻿using System;
using DDDSample1.Domain.Shared;
using System.Text.RegularExpressions;

namespace DDDSample1.Domain.Vehicles
{
    public class Vehicle : Entity<VehicleId>, IAggregateRoot
    {
        private readonly Regex regexRegistration = new Regex("^([0-9]{2}-[0-9]{2}-[A-Z]{2})$|^([0-9]{2}-[A-Z]{2}-[0-9]{2})$|^([A-Z]{2}-[0-9]{2}-[0-9]{2})$|^[A-Z]{2}[0-9]{2}[A-Z]{2}$");

        public string Registration { get; private set; }
        public string VIN { get; private set; }
        public string VehicleTypeCode { get; private set; }
        public DateTime DateOfServiceStart { get; private set; }

        private Vehicle()
        {

        }

        public Vehicle(string registration, string vin, string vehicleTypeCode, DateTime dateofServiceStart)
        {
            this.Id = new VehicleId(Guid.NewGuid());
            if (registration == null || registration.Length == 0)
            {
                throw new BusinessRuleValidationException("Every vehicle requires a registration.");
            }
            if (!regexRegistration.IsMatch(registration))
            {
                throw new BusinessRuleValidationException("Invalid registration!");
            }
            this.Registration = registration;
            if (vin == null || vin.Length == 0)
            {
                throw new BusinessRuleValidationException("Every vehicle requires a VIN.");
            }
            this.VIN = vin;
            if (vehicleTypeCode == null || vehicleTypeCode.Length == 0)
            {
                throw new BusinessRuleValidationException("Every vehicle requires a vehicle type.");
            }
            this.VehicleTypeCode = vehicleTypeCode;
            this.DateOfServiceStart = dateofServiceStart;
        }

        public void ChangeVehicleTypeCode(string vehicleTypeCode)
        {
            this.VehicleTypeCode = vehicleTypeCode;
        }
    }
}
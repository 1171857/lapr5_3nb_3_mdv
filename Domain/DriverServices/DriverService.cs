﻿using System;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.WorkBlocks;
using DDDSample1.Domain.Trips;
using System.Collections.Generic;

namespace DDDSample1.Domain.DriverServices
{
    public class DriverService : Entity<DriverServiceId>, IAggregateRoot
    {
        private readonly int MAX_HOUR_IN_SEC = 86400;
        private readonly int MIN_HOUR_IN_SEC = 0;

        public string DriverServiceIdentifier { get; private set; }
        public int StartTime { get; private set; }
        public int EndTime { get; private set; }
        public ICollection<WorkBlock> WorkBlocks { get; set; }
        public ICollection<Trip> Trips { get; private set; }

        private DriverService()
        {

        }

        public DriverService(string DriverServiceIdentifier, int startTime, int endTime, ICollection<WorkBlock> WorkBlocks)
        {
            this.Id = new DriverServiceId(Guid.NewGuid());
            if (startTime > MAX_HOUR_IN_SEC || startTime < MIN_HOUR_IN_SEC ||
                endTime > MAX_HOUR_IN_SEC || endTime < MIN_HOUR_IN_SEC)
            {
                throw new BusinessRuleValidationException("Invalid start or end time. Must be between 0 and 86400 seconds!");
            }
            this.DriverServiceIdentifier = DriverServiceIdentifier;
            this.StartTime = startTime;
            this.EndTime = endTime;
            this.WorkBlocks = WorkBlocks;
        }

        public void AddWorkBlocks(ICollection<WorkBlock> workblocks)
        {
            this.WorkBlocks = workblocks;
        }
    }
}
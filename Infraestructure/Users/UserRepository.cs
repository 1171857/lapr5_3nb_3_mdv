using DDDSample1.Domain.Users;
using DDDSample1.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

namespace DDDSample1.Infrastructure.Users
{
    public class UserRepository : BaseRepository<User, UserId>, IUserRepository
    {
        DDDSample1DbContext _context;
        public UserRepository(DDDSample1DbContext context) : base(context.Users)
        {
            this._context = context;
        }
        public async Task<User> GetByEmailAsync(string email)
        {
            return await this._context.Users.Where(x => email.Equals(x.Email)).FirstOrDefaultAsync();
        }

    }
}
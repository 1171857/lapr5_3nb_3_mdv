using Microsoft.EntityFrameworkCore;
using DDDSample1.Domain.Categories;
using DDDSample1.Domain.Products;
using DDDSample1.Domain.Families;
using DDDSample1.Domain.Vehicles;
using DDDSample1.Domain.WorkBlocks;
using DDDSample1.Domain.Drivers;
using DDDSample1.Infrastructure.Categories;
using DDDSample1.Infrastructure.Products;
using DDDSample1.Infrastructure.Vehicles;
using DDDSample1.Infrastructure.WorkBlocks;
using DDDSample1.Infrastructure.Trips;
using DDDNetCore.Infraestructure.Drivers;
using DDDSample1.Domain.Trips;
using DDDSample1.Infrastructure.VehicleServices;
using DDDSample1.Domain.VehicleServices;
using DDDSample1.Domain.Users;
using DDDSample1.Infrastructure.Users;
using DDDSample1.Domain.DriverServices;
using DDDSample1.Infrastructure.DriverServices;

namespace DDDSample1.Infrastructure
{
    public class DDDSample1DbContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Family> Families { get; set; }

        public DbSet<Vehicle> Vehicles { get; set; }

        public DbSet<WorkBlock> WorkBlocks { get; set; }

        public DbSet<Trip> Trips { get; set; }

        public DbSet<Driver> Drivers { get; set; }

        public DbSet<Domain.VehicleServices.VehicleService> VehicleServices { get; set; }

        public DbSet<DriverService> DriverServices { get; set; }

        public DbSet<User> Users { get; set; } 


        public DDDSample1DbContext(DbContextOptions options) : base(options)
        {
            this.Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CategoryEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ProductEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new FamilyEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new VehicleEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new WorkBlockEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new TripEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DriverEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new VehicleServiceEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new DriverServiceEntityTypeConfiguration());
            modelBuilder.ApplyConfiguration(new UserEntityTypeConfiguration());
            
        }
    }
}
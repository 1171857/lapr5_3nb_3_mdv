﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DDDSample1.Domain.Drivers;

namespace DDDNetCore.Infraestructure.Drivers
{
    internal class DriverEntityTypeConfiguration : IEntityTypeConfiguration<Driver>
    {
        public void Configure(EntityTypeBuilder<Driver> builder)
        {
            //builder.ToTable("Drivers", SchemaNames.DDDSample1);
            builder.HasKey(b => b.Id);
            builder.HasIndex(b => b.MechanographicalNumber).IsUnique();
            builder.HasIndex(b => b.Nif).IsUnique();
            builder.HasIndex(b => b.LicenseNumber).IsUnique();
        }
    }
}

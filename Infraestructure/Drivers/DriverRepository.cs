﻿using DDDSample1.Domain.Drivers;
using DDDSample1.Infrastructure;
using DDDSample1.Infrastructure.Shared;
using DDDSample1.Services.IRepositories;

namespace DDDNetCore.Infraestructure.Drivers
{
    public class DriverRepository : BaseRepository<Driver, DriverId>, IDriverRepository
    {
        public DriverRepository(DDDSample1DbContext context) : base(context.Drivers)
        {

        }
    }
}

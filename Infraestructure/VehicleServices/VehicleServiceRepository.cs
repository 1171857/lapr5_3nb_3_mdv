﻿using DDDSample1.Domain.VehicleServices;
using DDDSample1.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDDSample1.Infrastructure.VehicleServices
{
    public class VehicleServiceRepository : BaseRepository<VehicleService, VehicleServiceId>, IVehicleServiceRepository
    {
        DDDSample1DbContext _context;
        public VehicleServiceRepository(DDDSample1DbContext context) : base(context.VehicleServices)
        {
            this._context = context;
        }
       override
       public async Task<List<VehicleService>> GetAllAsync()
        {
            return await this._context.VehicleServices.Include("Trips").Include("WorkBlocks").ToListAsync();
        }
    }
}
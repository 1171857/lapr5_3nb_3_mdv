﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DDDSample1.Domain.VehicleServices;

namespace DDDSample1.Infrastructure.VehicleServices
{
    internal class VehicleServiceEntityTypeConfiguration : IEntityTypeConfiguration<VehicleService>
    {
        public void Configure(EntityTypeBuilder<VehicleService> builder)
        {
            //builder.ToTable("VehicleServices", SchemaNames.DDDSample1);
            builder.HasKey(b => b.Id);
            builder.HasIndex(b => b.VehicleServiceIdentifier).IsUnique();
        }
    }
}
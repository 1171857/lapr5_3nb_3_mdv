﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DDDSample1.Domain.Vehicles;

namespace DDDSample1.Infrastructure.Vehicles
{
    internal class VehicleEntityTypeConfiguration : IEntityTypeConfiguration<Vehicle>
    {
        public void Configure(EntityTypeBuilder<Vehicle> builder)
        {
            //builder.ToTable("Vehicles", SchemaNames.DDDSample1);
            builder.HasKey(b => b.Id);
            builder.HasIndex(b => b.Registration).IsUnique();
            builder.HasIndex(b => b.VIN).IsUnique();
        }
    }
}
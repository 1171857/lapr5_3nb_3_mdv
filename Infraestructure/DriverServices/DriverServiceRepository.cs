﻿using DDDSample1.Domain.DriverServices;
using DDDSample1.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DDDSample1.Infrastructure.DriverServices
{
    public class DriverServiceRepository : BaseRepository<DriverService, DriverServiceId>, IDriverServiceRepository
    {
        DDDSample1DbContext _context;
        public DriverServiceRepository(DDDSample1DbContext context) : base(context.DriverServices)
        {
            this._context = context;
        }
       override

       public async Task<List<DriverService>> GetAllAsync()
        {
            return await this._context.DriverServices.Include("Trips").Include("WorkBlocks").ToListAsync();
        }
    }
}
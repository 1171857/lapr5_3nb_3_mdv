﻿using DDDSample1.Domain.DriverServices;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DDDSample1.Infrastructure.DriverServices
{
    internal class DriverServiceEntityTypeConfiguration : IEntityTypeConfiguration<DriverService>
    {
        public void Configure(EntityTypeBuilder<DriverService> builder)
        {
            //builder.ToTable("DriverServices", SchemaNames.DDDSample1);
            builder.HasKey(b => b.Id);
            builder.HasIndex(b => b.DriverServiceIdentifier).IsUnique();
        }
    }
}
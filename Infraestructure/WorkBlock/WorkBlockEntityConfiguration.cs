using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DDDSample1.Domain.WorkBlocks;

namespace DDDSample1.Infrastructure.WorkBlocks
{
    internal class WorkBlockEntityTypeConfiguration : IEntityTypeConfiguration<WorkBlock>
    {
        public void Configure(EntityTypeBuilder<WorkBlock> builder)
        {
            //builder.ToTable("WorkBlocks", SchemaNames.DDDSample1);
            builder.HasKey(b => b.Id);
        }
    }
}
using DDDSample1.Domain.WorkBlocks;
using DDDSample1.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDDSample1.Infrastructure.WorkBlocks
{
    public class WorkBlockRepository : BaseRepository<WorkBlock, WorkBlockId>, IWorkBlockRepository
    {
        DDDSample1DbContext _context;
        public WorkBlockRepository(DDDSample1DbContext context) : base(context.WorkBlocks)
        {
            this._context = context;
        }
        override
        public async Task<List<WorkBlock>> GetAllAsync()
        {
            return await this._context.WorkBlocks.Include("Trips").Include("VehicleService").ToListAsync();
        }

        public async Task<WorkBlock> GetByIdentifierAsync(String identifier)
        {
            //return await this._context.Categories.FindAsync(id);
            return await this._context.WorkBlocks
                .Where(x => identifier.Equals(x.WorkBlockIdentifier)).FirstOrDefaultAsync();
        }

        override
        public async Task<WorkBlock> GetByIdAsync(WorkBlockId id)
        {
            return await this._context.WorkBlocks.Include("Trips").Include("VehicleService").Where(x => id.Equals(x.Id)).FirstOrDefaultAsync();
        }
    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DDDSample1.Domain.Trips;
using DDDSample1.Infrastructure.Shared;
using Microsoft.EntityFrameworkCore;

namespace DDDSample1.Infrastructure.Trips
{
    public class TripRepository : BaseRepository<Trip, TripId>, ITripRepository
    {
        DDDSample1DbContext _context;
        public TripRepository(DDDSample1DbContext context) :base(context.Trips)
        {
            this._context = context;
        }

        public async Task<Trip> GetByIdentifierAsync(String identifier)
        {
            //return await this._context.Categories.FindAsync(id);
            return await this._context.Trips
                .Where(x => identifier.Equals(x.TripIdentifier)).FirstOrDefaultAsync();
        }
    }
}
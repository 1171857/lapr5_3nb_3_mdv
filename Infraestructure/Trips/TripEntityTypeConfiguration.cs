﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using DDDSample1.Domain.Trips;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace DDDSample1.Infrastructure.Trips
{
    internal class TripEntityTypeConfiguration : IEntityTypeConfiguration<Trip>
    {
        public void Configure(EntityTypeBuilder<Trip> builder)
        {
            //builder.ToTable("Trips", SchemaNames.DDDSample1);
            builder.HasKey(b => b.Id);
            builder.HasIndex(b => b.TripIdentifier).IsUnique();
            //builder.OwnsOne(x => x.Schedules);
            var splitStringConverter = new ValueConverter<List<Schedule>, string>(v => JsonConvert.SerializeObject(v), v => JsonConvert.DeserializeObject<List<Schedule>>(v​));
            builder.Property(nameof(Trip.Schedules)).HasConversion(splitStringConverter);
        }
    }
}
﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Npgsql;
using DDDSample1.Infrastructure;
using DDDSample1.Infrastructure.Categories;
using DDDSample1.Infrastructure.Products;
using DDDSample1.Infrastructure.Families;
using DDDSample1.Infrastructure.Shared;
using DDDSample1.Infrastructure.Vehicles;
using DDDSample1.Infrastructure.WorkBlocks;
using DDDNetCore.Infraestructure.Drivers;
using DDDSample1.Infrastructure.VehicleServices;
using DDDSample1.Domain.Shared;
using DDDSample1.Domain.Categories;
using DDDSample1.Domain.Products;
using DDDSample1.Domain.Families;
using DDDSample1.Domain.Vehicles;
using DDDSample1.Domain.WorkBlocks;
using DDDSample1.Domain.VehicleServices;
using DDDSample1.Infrastructure.Trips;
using DDDSample1.Domain.Trips;
using DDDSample1.Services;
using DDDSample1.Services.IRepositories;
using Microsoft.OpenApi.Models;
using System;
using DDDSample1.Infrastructure.Users;
using DDDSample1.Domain.Users;
using DDDSample1.Infrastructure.DriverServices;
using DDDSample1.Infrastructure.Auth.Jwt;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;

namespace DDDSample1
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Use in-memory database
            /*services.AddDbContext<DDDSample1DbContext>(opt =>
                opt.UseInMemoryDatabase("DDDSample1DB")
                .ReplaceService<IValueConverterSelector, StronglyEntityIdValueConverterSelector>());*/

            // Use SQL Server or Azure SQL database
            /*services.AddDbContext<DDDSample1DbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("MyDbConnection"))
                .ReplaceService<IValueConverterSelector, StronglyEntityIdValueConverterSelector>());*/

            // Use PostgreSQL database
            services.AddDbContext<DDDSample1DbContext>(options =>
                options.UseNpgsql(BuildNpgsqlConnectionString())
                .ReplaceService<IValueConverterSelector, StronglyEntityIdValueConverterSelector>());

            // Enable CORS
            services.AddCors(); // Make sure you call this previous to AddMvc

            // Configure A&A
            ConfigureAuthenticationServices(services);

            ConfigureMyServices(services);


            services.AddControllers().AddNewtonsoftJson();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo 
                { 
                    Title = "MDV Api", 
                    Version = "v1",
                    Description = "The MDV API",
                    TermsOfService = new Uri("https://example.com/terms"),
                    Contact = new OpenApiContact
                    {
                        Name = "Grupo 87",
                        Email = string.Empty,
                        Url = new Uri("https://twitter.com/spboyer"),
                    },
                    License = new OpenApiLicense
                    {
                        Name = "Use under DEI.ISEP.IPP.PT",
                        Url = new Uri("https://example.com/license"),
                    }
                });
            });



        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseCors(builder => builder.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            // Swagger config
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "MDV API v1");
                c.RoutePrefix = string.Empty;
            });
        }

        public void ConfigureMyServices(IServiceCollection services)
        {
            services.AddTransient<IUnitOfWork, UnitOfWork>();

            services.AddTransient<ICategoryRepository, CategoryRepository>();
            services.AddTransient<CategoryService>();

            services.AddTransient<IProductRepository, ProductRepository>();
            services.AddTransient<ProductService>();

            services.AddTransient<IFamilyRepository, FamilyRepository>();
            services.AddTransient<FamilyService>();

            services.AddTransient<IVehicleRepository, VehicleRepository>();
            services.AddTransient<Domain.Vehicles.VehicleService>();

            services.AddTransient<IWorkBlockRepository, WorkBlockRepository>();
            services.AddTransient<WorkBlockService>();

            services.AddTransient<ITripRepository, TripRepository>();
            services.AddTransient<TripService>();

            services.AddTransient<IDriverRepository, DriverRepository>();
            services.AddTransient<DriverService>();

            services.AddTransient<IVehicleServiceRepository, VehicleServiceRepository>();
            services.AddTransient<VehicleServiceService>();

            services.AddTransient<Domain.DriverServices.IDriverServiceRepository, DriverServiceRepository>();
            services.AddTransient<Domain.DriverServices.DriverServiceService>();

            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<UserService>();

            services.AddTransient<FileService>();
        }

        private string BuildNpgsqlConnectionString()
        {
            var databaseUrl = System.Environment.GetEnvironmentVariable("DATABASE_URL")
                ?? Configuration.GetConnectionString("MyPostgreSqlUrl");
            var databaseUri = new System.Uri(databaseUrl);
            var userInfo = databaseUri.UserInfo.Split(':');

            var builder = new NpgsqlConnectionStringBuilder
            {
                Host = databaseUri.Host,
                Port = databaseUri.Port,
                Username = userInfo[0],
                Password = userInfo[1],
                Database = databaseUri.LocalPath.TrimStart('/'),
                SslMode = Npgsql.SslMode.Require,
                TrustServerCertificate = true,
            };

            return builder.ToString();
        }

        public void ConfigureAuthenticationServices(IServiceCollection services)
        {
            var jwtTokenConfig = Configuration.GetSection("jwtTokenConfig").Get<JwtTokenConfig>();
            
            services.AddSingleton(jwtTokenConfig);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = true;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuer = true,
                    ValidIssuer = jwtTokenConfig.Issuer,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(jwtTokenConfig.Secret)),
                    ValidAudience = jwtTokenConfig.Audience,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    // token expires 1 minute later
                    ClockSkew = TimeSpan.FromMinutes(1)
                };
            });

            services.AddSingleton<IJwtAuthManager, JwtAuthManager>();
            services.AddHostedService<JwtRefreshTokenCache>();
            services.AddScoped<UserService, UserService>();
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DDDNetCore.Migrations
{
    public partial class RefactorDriversFields : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ExitDateFromCompany",
                table: "Drivers",
                newName: "StartDate");

            migrationBuilder.RenameColumn(
                name: "EntryDateInCompany",
                table: "Drivers",
                newName: "LicenseExpirationDate");

            migrationBuilder.RenameColumn(
                name: "DriverLicenseNumber",
                table: "Drivers",
                newName: "LicenseNumber");

            migrationBuilder.RenameColumn(
                name: "DriverLicenseExpirationDate",
                table: "Drivers",
                newName: "ExitDate");

            migrationBuilder.RenameIndex(
                name: "IX_Drivers_DriverLicenseNumber",
                table: "Drivers",
                newName: "IX_Drivers_LicenseNumber");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "StartDate",
                table: "Drivers",
                newName: "ExitDateFromCompany");

            migrationBuilder.RenameColumn(
                name: "LicenseNumber",
                table: "Drivers",
                newName: "DriverLicenseNumber");

            migrationBuilder.RenameColumn(
                name: "LicenseExpirationDate",
                table: "Drivers",
                newName: "EntryDateInCompany");

            migrationBuilder.RenameColumn(
                name: "ExitDate",
                table: "Drivers",
                newName: "DriverLicenseExpirationDate");

            migrationBuilder.RenameIndex(
                name: "IX_Drivers_LicenseNumber",
                table: "Drivers",
                newName: "IX_Drivers_DriverLicenseNumber");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DDDNetCore.Migrations
{
    public partial class VehicleService_N3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TripVehicleService_VehicleServices_DriverServicesId",
                table: "TripVehicleService");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TripVehicleService",
                table: "TripVehicleService");

            migrationBuilder.DropIndex(
                name: "IX_TripVehicleService_TripsId",
                table: "TripVehicleService");

            migrationBuilder.RenameColumn(
                name: "DriverServicesId",
                table: "TripVehicleService",
                newName: "VehicleServicesId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TripVehicleService",
                table: "TripVehicleService",
                columns: new[] { "TripsId", "VehicleServicesId" });

            migrationBuilder.CreateIndex(
                name: "IX_TripVehicleService_VehicleServicesId",
                table: "TripVehicleService",
                column: "VehicleServicesId");

            migrationBuilder.AddForeignKey(
                name: "FK_TripVehicleService_VehicleServices_VehicleServicesId",
                table: "TripVehicleService",
                column: "VehicleServicesId",
                principalTable: "VehicleServices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TripVehicleService_VehicleServices_VehicleServicesId",
                table: "TripVehicleService");

            migrationBuilder.DropPrimaryKey(
                name: "PK_TripVehicleService",
                table: "TripVehicleService");

            migrationBuilder.DropIndex(
                name: "IX_TripVehicleService_VehicleServicesId",
                table: "TripVehicleService");

            migrationBuilder.RenameColumn(
                name: "VehicleServicesId",
                table: "TripVehicleService",
                newName: "DriverServicesId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_TripVehicleService",
                table: "TripVehicleService",
                columns: new[] { "DriverServicesId", "TripsId" });

            migrationBuilder.CreateIndex(
                name: "IX_TripVehicleService_TripsId",
                table: "TripVehicleService",
                column: "TripsId");

            migrationBuilder.AddForeignKey(
                name: "FK_TripVehicleService_VehicleServices_DriverServicesId",
                table: "TripVehicleService",
                column: "DriverServicesId",
                principalTable: "VehicleServices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}

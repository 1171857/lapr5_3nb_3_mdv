﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DDDNetCore.Migrations
{
    public partial class VehicleService : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "vehicleServiceId",
                table: "WorkBlocks",
                type: "text",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "VehicleServices",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    StartTime = table.Column<int>(type: "integer", nullable: false),
                    EndTime = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleServices", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WorkBlocks_vehicleServiceId",
                table: "WorkBlocks",
                column: "vehicleServiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_WorkBlocks_VehicleServices_vehicleServiceId",
                table: "WorkBlocks",
                column: "vehicleServiceId",
                principalTable: "VehicleServices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WorkBlocks_VehicleServices_vehicleServiceId",
                table: "WorkBlocks");

            migrationBuilder.DropTable(
                name: "VehicleServices");

            migrationBuilder.DropIndex(
                name: "IX_WorkBlocks_vehicleServiceId",
                table: "WorkBlocks");

            migrationBuilder.DropColumn(
                name: "vehicleServiceId",
                table: "WorkBlocks");
        }
    }
}

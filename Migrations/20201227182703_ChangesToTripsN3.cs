﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DDDNetCore.Migrations
{
    public partial class ChangesToTripsN3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Schedules_Capacity",
                table: "Trips");

            migrationBuilder.AddColumn<List<int>>(
                name: "Schedule",
                table: "Trips",
                type: "integer[]",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Schedule",
                table: "Trips");

            migrationBuilder.AddColumn<int>(
                name: "Schedules_Capacity",
                table: "Trips",
                type: "integer",
                nullable: true);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DDDNetCore.Migrations
{
    public partial class VehicleServices_N5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_VehicleServices_VehicleServiceIdentifier",
                table: "VehicleServices",
                column: "VehicleServiceIdentifier",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_VehicleServices_VehicleServiceIdentifier",
                table: "VehicleServices");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DDDNetCore.Migrations
{
    public partial class VehicleServices_N4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WorkBlocks_VehicleServices_vehicleServiceId",
                table: "WorkBlocks");

            migrationBuilder.RenameColumn(
                name: "vehicleServiceId",
                table: "WorkBlocks",
                newName: "VehicleServiceId");

            migrationBuilder.RenameIndex(
                name: "IX_WorkBlocks_vehicleServiceId",
                table: "WorkBlocks",
                newName: "IX_WorkBlocks_VehicleServiceId");

            migrationBuilder.AddColumn<string>(
                name: "VehicleServiceIdentifier",
                table: "VehicleServices",
                type: "text",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_WorkBlocks_VehicleServices_VehicleServiceId",
                table: "WorkBlocks",
                column: "VehicleServiceId",
                principalTable: "VehicleServices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_WorkBlocks_VehicleServices_VehicleServiceId",
                table: "WorkBlocks");

            migrationBuilder.DropColumn(
                name: "VehicleServiceIdentifier",
                table: "VehicleServices");

            migrationBuilder.RenameColumn(
                name: "VehicleServiceId",
                table: "WorkBlocks",
                newName: "vehicleServiceId");

            migrationBuilder.RenameIndex(
                name: "IX_WorkBlocks_VehicleServiceId",
                table: "WorkBlocks",
                newName: "IX_WorkBlocks_vehicleServiceId");

            migrationBuilder.AddForeignKey(
                name: "FK_WorkBlocks_VehicleServices_vehicleServiceId",
                table: "WorkBlocks",
                column: "vehicleServiceId",
                principalTable: "VehicleServices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}

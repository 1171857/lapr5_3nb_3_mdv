﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DDDNetCore.Migrations
{
    public partial class WorkBlock_N2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "WorkBlockHourLimite",
                table: "WorkBlocks",
                newName: "WorkBlockHourLimit");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "WorkBlockHourLimit",
                table: "WorkBlocks",
                newName: "WorkBlockHourLimite");
        }
    }
}

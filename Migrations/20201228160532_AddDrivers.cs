﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DDDNetCore.Migrations
{
    public partial class AddDrivers : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Drivers",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    MechanographicalNumber = table.Column<string>(type: "text", nullable: true),
                    Name = table.Column<string>(type: "text", nullable: true),
                    BirthDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    Nif = table.Column<string>(type: "text", nullable: true),
                    DriverLicenseNumber = table.Column<string>(type: "text", nullable: true),
                    DriverLicenseExpirationDate = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    EntryDateInCompany = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    ExitDateFromCompany = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    DriverTypes = table.Column<List<string>>(type: "text[]", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Drivers", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Drivers_DriverLicenseNumber",
                table: "Drivers",
                column: "DriverLicenseNumber",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Drivers_MechanographicalNumber",
                table: "Drivers",
                column: "MechanographicalNumber",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Drivers_Nif",
                table: "Drivers",
                column: "Nif",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Drivers");
        }
    }
}

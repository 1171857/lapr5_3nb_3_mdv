﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DDDNetCore.Migrations
{
    public partial class ChangesToTripsN6 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Schedules_Capacity",
                table: "Trips");

            migrationBuilder.AddColumn<string>(
                name: "Schedules",
                table: "Trips",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Schedules",
                table: "Trips");

            migrationBuilder.AddColumn<int>(
                name: "Schedules_Capacity",
                table: "Trips",
                type: "integer",
                nullable: true);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DDDNetCore.Migrations
{
    public partial class VehicleService_N2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TripVehicleService",
                columns: table => new
                {
                    DriverServicesId = table.Column<string>(type: "text", nullable: false),
                    TripsId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TripVehicleService", x => new { x.DriverServicesId, x.TripsId });
                    table.ForeignKey(
                        name: "FK_TripVehicleService_Trips_TripsId",
                        column: x => x.TripsId,
                        principalTable: "Trips",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TripVehicleService_VehicleServices_DriverServicesId",
                        column: x => x.DriverServicesId,
                        principalTable: "VehicleServices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TripVehicleService_TripsId",
                table: "TripVehicleService",
                column: "TripsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TripVehicleService");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DDDNetCore.Migrations
{
    public partial class ChangesToTripsN4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Nodes",
                table: "Trips");

            migrationBuilder.DropColumn(
                name: "Schedule",
                table: "Trips");

            migrationBuilder.AddColumn<int>(
                name: "Schedule_Capacity",
                table: "Trips",
                type: "integer",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Schedule_Capacity",
                table: "Trips");

            migrationBuilder.AddColumn<string[]>(
                name: "Nodes",
                table: "Trips",
                type: "text[]",
                nullable: true);

            migrationBuilder.AddColumn<int[]>(
                name: "Schedule",
                table: "Trips",
                type: "integer[]",
                nullable: true);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DDDNetCore.Migrations
{
    public partial class DriverService : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DriverServiceId",
                table: "WorkBlocks",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DriverServiceId",
                table: "Trips",
                type: "text",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "DriverServices",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    DriverServiceIdentifier = table.Column<string>(type: "text", nullable: true),
                    StartTime = table.Column<int>(type: "integer", nullable: false),
                    EndTime = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DriverServices", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_WorkBlocks_DriverServiceId",
                table: "WorkBlocks",
                column: "DriverServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_Trips_DriverServiceId",
                table: "Trips",
                column: "DriverServiceId");

            migrationBuilder.CreateIndex(
                name: "IX_DriverServices_DriverServiceIdentifier",
                table: "DriverServices",
                column: "DriverServiceIdentifier",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Trips_DriverServices_DriverServiceId",
                table: "Trips",
                column: "DriverServiceId",
                principalTable: "DriverServices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_WorkBlocks_DriverServices_DriverServiceId",
                table: "WorkBlocks",
                column: "DriverServiceId",
                principalTable: "DriverServices",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Trips_DriverServices_DriverServiceId",
                table: "Trips");

            migrationBuilder.DropForeignKey(
                name: "FK_WorkBlocks_DriverServices_DriverServiceId",
                table: "WorkBlocks");

            migrationBuilder.DropTable(
                name: "DriverServices");

            migrationBuilder.DropIndex(
                name: "IX_WorkBlocks_DriverServiceId",
                table: "WorkBlocks");

            migrationBuilder.DropIndex(
                name: "IX_Trips_DriverServiceId",
                table: "Trips");

            migrationBuilder.DropColumn(
                name: "DriverServiceId",
                table: "WorkBlocks");

            migrationBuilder.DropColumn(
                name: "DriverServiceId",
                table: "Trips");
        }
    }
}

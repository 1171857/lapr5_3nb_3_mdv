﻿using System.Collections.Generic;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DDDNetCore.Migrations
{
    public partial class WorkBlocks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Trips",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    TripIdentifier = table.Column<string>(type: "text", nullable: true),
                    Orientation = table.Column<string>(type: "text", nullable: true),
                    LineId = table.Column<string>(type: "text", nullable: true),
                    RouteId = table.Column<string>(type: "text", nullable: true),
                    Schedules = table.Column<List<int>>(type: "integer[]", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Trips", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "WorkBlocks",
                columns: table => new
                {
                    Id = table.Column<string>(type: "text", nullable: false),
                    WorkBlockIdentifier = table.Column<string>(type: "text", nullable: true),
                    StartTime = table.Column<int>(type: "integer", nullable: false),
                    EndTime = table.Column<int>(type: "integer", nullable: false),
                    WorkBlockHourLimite = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkBlocks", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "TripWorkBlock",
                columns: table => new
                {
                    TripsId = table.Column<string>(type: "text", nullable: false),
                    WorkBlocksId = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TripWorkBlock", x => new { x.TripsId, x.WorkBlocksId });
                    table.ForeignKey(
                        name: "FK_TripWorkBlock_Trips_TripsId",
                        column: x => x.TripsId,
                        principalTable: "Trips",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TripWorkBlock_WorkBlocks_WorkBlocksId",
                        column: x => x.WorkBlocksId,
                        principalTable: "WorkBlocks",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TripWorkBlock_WorkBlocksId",
                table: "TripWorkBlock",
                column: "WorkBlocksId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TripWorkBlock");

            migrationBuilder.DropTable(
                name: "Trips");

            migrationBuilder.DropTable(
                name: "WorkBlocks");
        }
    }
}

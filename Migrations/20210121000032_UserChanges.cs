﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DDDNetCore.Migrations
{
    public partial class UserChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "AcceptanceOfPrivacyPolitic",
                table: "Users",
                newName: "AcceptanceOfPrivacyPolicy");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "AcceptanceOfPrivacyPolicy",
                table: "Users",
                newName: "AcceptanceOfPrivacyPolitic");
        }
    }
}

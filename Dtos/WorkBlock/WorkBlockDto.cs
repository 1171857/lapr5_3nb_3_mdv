using DDDSample1.Domain.Trips;
using DDDSample1.Domain.VehicleServices;
using System;
using System.Collections.Generic;

namespace DDDSample1.Domain.WorkBlocks
{
    public class WorkBlockDto
    {
        public Guid Id { get; private set; }

        public string WorkBlockIdentifier { get; private set; }

        public int StartTime { get; private set; }

        public int EndTime { get; private set; }

        public ICollection<String> ListTrips { get; private set; }

        public string VehicleService { get; private set; }

        public WorkBlockDto(Guid id, string workBlockIdentifier, int startTime, int endTime, ICollection<Trip> listTrips, VehicleService vehicleService)
        {
            this.Id = id;
            this.WorkBlockIdentifier = workBlockIdentifier;
            this.StartTime = startTime;
            this.EndTime = endTime;
            this.ListTrips = convertListTripIdToString(listTrips);
            if (vehicleService != null)
            {
                this.VehicleService = vehicleService.Id.Value;
            }
        }

        public List<String> convertListTripIdToString(ICollection<Trip> ListTrips)
        {
            List<String> NewList = new List<String>();
            if (ListTrips != null)
            {
                foreach (Trip element in ListTrips)
                {
                    NewList.Add(element.Id.Value);
                }
            }
            return NewList;
        }
    }
}
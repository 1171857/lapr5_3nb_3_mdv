using DDDSample1.Domain.Trips;
using DDDSample1.Domain.VehicleServices;
using System;
using System.Collections.Generic;

namespace DDDSample1.Domain.WorkBlocks
{
    public class CreatingWorkBlockDto
    {
        public string WorkBlockIdentifier { get; set; }

        public int StartTime { get; set; }

        public int EndTime { get; set; }

        public List<TripId> ListTrips { get; set; }

        public VehicleServiceId VehicleServiceId { get; set; }

        public CreatingWorkBlockDto()
        {
            this.ListTrips = new List<TripId>();
        }
    }
}
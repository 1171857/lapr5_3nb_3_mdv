using DDDSample1.Domain.Shared;
using System;

namespace DDDSample1.Domain.Users
{
    public class CreatingUserDto
    {
        public string Name { get; private set; }
        public string Email { get; private set; }
        public string Password { get; private set; }
        public Role Role { get; private set; }

        public CreatingUserDto(string name, string email, string password, string role)
        {
            if(Enum.IsDefined(typeof(Role), role))
            {
                this.Role = (Role) Enum.Parse(typeof(Role),role);
            }
            else
            {
                throw new BusinessRuleValidationException("Invalid user Role!");
            }
            this.Name = name;
            this.Email = email;
            this.Password = password;
        }
    }
}
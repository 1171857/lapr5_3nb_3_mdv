using System;

namespace DDDSample1.Domain.Users
{
    public class UserDto
    {
        public Guid Id { get; private set; }
        public string Name { get; private set; }
        public string Email { get; private set; }
        public string Password { get; private set; }
        public DateTime AcceptanceOfPrivacyPolicy { get; private set; }
        public DateTime LastLogin { get; private set; }
        public string Role { get; private set; }

        public UserDto(Guid id, string name, string email, string password, DateTime acceptanceOfPrivacyPolitic, DateTime lastLogin, Role role)
        {
            this.Id = id;
            this.Name = name;
            this.Email = email;
            this.Password = password;
            this.AcceptanceOfPrivacyPolicy = acceptanceOfPrivacyPolitic;
            this.LastLogin = lastLogin;
            this.Role = role.ToString();
        }

    }
}
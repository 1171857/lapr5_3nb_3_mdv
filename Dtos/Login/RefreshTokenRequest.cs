using System.Text.Json.Serialization;

namespace DDDSample1.Dtos.Login
{
    public class RefreshTokenRequest
    {
        [JsonPropertyName("refreshToken")]
        public string RefreshToken { get; set; }
    }
}
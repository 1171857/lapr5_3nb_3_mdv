﻿using DDDSample1.Domain.Trips;
using DDDSample1.Domain.WorkBlocks;
using System.Collections.Generic;

namespace DDDSample1.Domain.DriverServices
{
    public class CreatingDriverServiceDto
    {
        public string DriverServiceIdentifier { get; set; }

        public int StartTime { get; set; }

        public int EndTime { get; set; }

        public List<WorkBlockId> ListWorkBlocks { get; set; }

        public CreatingDriverServiceDto() 
        {
            this.ListWorkBlocks = new List<WorkBlockId>();
        }
    }
}
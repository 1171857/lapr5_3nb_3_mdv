﻿using DDDSample1.Domain.Trips;
using DDDSample1.Domain.WorkBlocks;
using System;
using System.Collections.Generic;

namespace DDDSample1.Domain.DriverServices
{
    public class DriverServiceDto
    {
        public Guid Id { get; private set; }

        public string DriverServiceIdentifier { get; private set; }
       
        public int StartTime { get; private set; }

        public int EndTime { get; private set; }

        public ICollection<String> ListWorkblocks { get; private set; }

        public DriverServiceDto(Guid id, string DriverServiceIdentifier, int startTime, int endTime, ICollection<WorkBlock> listWorkblocks)
        {
            this.Id = id;
            this.DriverServiceIdentifier = DriverServiceIdentifier;
            this.StartTime = startTime;
            this.EndTime = endTime;
            this.ListWorkblocks = convertListWorkblockIdToString(listWorkblocks);
        }

        public List<String> convertListTripIdToString(ICollection<Trip> ListTrips)
        {
            List<String> NewList = new List<String>();
            foreach (Trip element in ListTrips)
            {
                NewList.Add(element.Id.Value);
            }
            return NewList;
        }

        public List<String> convertListWorkblockIdToString(ICollection<WorkBlock> listWorkblock)
        {
            List<String> NewList = new List<String>();
            if (listWorkblock != null)
            {
                foreach (WorkBlock element in listWorkblock)
                {
                    NewList.Add(element.Id.Value);
                }
            }
                return NewList;
        }
    }
}
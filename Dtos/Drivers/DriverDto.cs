﻿using System;
using System.Collections.Generic;

namespace DDDSample1.Dtos.Drivers
{
    public class DriverDto
    {
        public Guid Id { get; set; }
        public string MechanographicalNumber { get; private set; }
        public string Name { get; private set; }
        public DateTime BirthDate { get; private set; }
        public string Nif { get; private set; }
        public string LicenseNumber { get; private set; }
        public DateTime LicenseExpirationDate { get; private set; }
        public DateTime StartDate { get; private set; }
        public DateTime ExitDate { get; private set; }
        public List<string> DriverTypes { get; private set; }

        public DriverDto(Guid Id, string mechanographicalNumber, string name,
            DateTime birthDate, string nif, string licenseNumber,
            DateTime licenseExpirationDate, DateTime startDate,
            DateTime exitDate, List<string> driverTypes)
        {
            this.Id = Id;
            this.MechanographicalNumber = mechanographicalNumber;
            this.Name = name;
            this.BirthDate = birthDate;
            this.Nif = nif;
            this.LicenseNumber = licenseNumber;
            this.LicenseExpirationDate = licenseExpirationDate;
            this.StartDate = startDate;
            this.ExitDate = exitDate;
            this.DriverTypes = driverTypes;
        }
    }
}
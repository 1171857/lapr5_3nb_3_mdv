﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDDSample1.Domain.Routes
{
    public class RouteDto
    {
        public string Id { get; set; }
        public string LineCode { get; set; }
        public string Type{ get; set; }
        public List<RouteSegmentDto> RouteSegments { get; set; }
    }
}

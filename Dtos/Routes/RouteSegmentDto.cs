﻿using System;

namespace DDDSample1.Domain.Routes
{
    public class RouteSegmentDto
    {
        public string StartNode { get; set; }
        public string EndNode { get; set; }
        public int Duration { get; set; }
        public int Distance { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DDDSample1.Domain.Trips
{
    public class TripGenerationDto
    {
        public bool SingleTrip { get; set; }
        public string Route { get; set; }
        public string TripIdentifier { get; set; }

        //Single & Multi Gen Params
        public string LineId { get; set; }
        public int StartTime { get; set; }

        // Multi gen params
        public int EndTime { get; set; }
        public int Frequency { get; set; }
        public int NTrips { get; set; }
        public string RouteOutward { get; set; }
        public string RouteBackward { get; set; }
        public int MaxVeichlesInParallel { get; set; }

    }
}

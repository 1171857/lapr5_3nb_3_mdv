
namespace DDDSample1.Domain.Trips
{
    public class CreatingScheduleDto
    {
        public string NodeId { get; private set; }

        public int TimeInSeconds { get; private set; }


        public CreatingScheduleDto(string nodeId, int timeInSeconds)
        {
            this.NodeId = nodeId;
            this.TimeInSeconds = timeInSeconds;
        }
    }
}
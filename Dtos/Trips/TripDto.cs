using DDDSample1.Domain.Trips;
using DDDSample1.Domain.WorkBlocks;
using System;
using System.Collections.Generic;

namespace DDDSample1.Domain.Trips
{
    public class TripDto
    {
        public Guid Id { get; set; }

        public string TripIdentifier { get; private set; }

        public string LineId { get; private set; }

        public string RouteId { get; private set; }

        public List<Schedule> Schedule { get; private set; }

        //public List<string> Nodes { get; private set; }


        public TripDto(Guid id, string tripIdentifier, string lineId, string routeId, List<Schedule> schedule)
        {
            this.Id = id;
            this.TripIdentifier = tripIdentifier;
            this.LineId = lineId;
            this.RouteId = routeId;
            this.Schedule = schedule;
            //this.Nodes = nodes;
        }

        private List<string> createListSchedules(List<Schedule> schedules)
        {
            List<string> newList = new List<string>();
            foreach(Schedule element in schedules)
            {
                newList.Add(element.ToString());
            }
            return newList;
        }
    }
}
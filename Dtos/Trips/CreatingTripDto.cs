using System;
using System.Collections.Generic;

namespace DDDSample1.Domain.Trips
{
    public class CreatingTripDto
    {
        public string TripIdentifier { get; private set; }

        public string LineId { get; private set; }

        public string RouteId { get; private set; }

        public List<Schedule> Schedules { get; private set; }

        //public List<string> Nodes { get; private set; }


        public CreatingTripDto(string tripIdentifier, string orientation, string lineId, string routeId, List<CreatingScheduleDto> schedule)
        {
            this.TripIdentifier = tripIdentifier;
            this.LineId = lineId;
            this.RouteId = routeId;
            this.Schedules = new List<Schedule>();
            foreach(CreatingScheduleDto element in schedule)
            {
                this.Schedules.Add(new Schedule(element.NodeId, element.TimeInSeconds));
            }
            //this.Nodes = nodes;
        }
    }
}
﻿using DDDSample1.Domain.Trips;
using DDDSample1.Domain.WorkBlocks;
using System;
using System.Collections.Generic;

namespace DDDSample1.Domain.VehicleServices
{
    public class VehicleServiceDto
    {
        public Guid Id { get; private set; }

        public string VehicleServiceIdentifier { get; private set; }
       
        public int StartTime { get; private set; }

        public int EndTime { get; private set; }

        public ICollection<String> ListTrips { get; private set; }

        public ICollection<String> ListWorkblocks { get; private set; }

        public VehicleServiceDto(Guid id, string vehicleServiceIdentifier, int startTime, int endTime, ICollection<Trip> listTrips, ICollection<WorkBlock> listWorkblocks)
        {
            this.Id = id;
            this.VehicleServiceIdentifier = vehicleServiceIdentifier;
            this.StartTime = startTime;
            this.EndTime = endTime;
            this.ListTrips = convertListTripIdToString(listTrips);
            this.ListWorkblocks = convertListWorkblockIdToString(listWorkblocks);
        }

        public List<String> convertListTripIdToString(ICollection<Trip> ListTrips)
        {
            List<String> NewList = new List<String>();
            foreach (Trip element in ListTrips)
            {
                NewList.Add(element.Id.Value);
            }
            return NewList;
        }

        public List<String> convertListWorkblockIdToString(ICollection<WorkBlock> listWorkblock)
        {
            List<String> NewList = new List<String>();
            if (listWorkblock != null)
            {
                foreach (WorkBlock element in listWorkblock)
                {
                    NewList.Add(element.Id.Value);
                }
            }
                return NewList;
        }
    }
}
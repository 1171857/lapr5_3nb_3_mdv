﻿using DDDSample1.Domain.Trips;
using DDDSample1.Domain.WorkBlocks;
using System.Collections.Generic;

namespace DDDSample1.Domain.VehicleServices
{
    public class CreatingVehicleServiceDto
    {
        public string VehicleServiceIdentifier { get; set; }

        public int StartTime { get; set; }

        public int EndTime { get; set; }

        public List<TripId> ListTrips { get; set; }

        public List<WorkBlockId> ListWorkBlocks { get; set; }

        public CreatingVehicleServiceDto()
        {
            this.ListTrips = new List<TripId>();
            this.ListWorkBlocks = new List<WorkBlockId>();
        }
    }
}
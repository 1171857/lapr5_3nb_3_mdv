﻿using System;

namespace DDDSample1.Domain.Vehicles
{
    public class VehicleDto
    {
        public Guid Id { get; set; }
        public string Registration { get; set; }
        public string VIN { get; set; }
        public string VehicleTypeCode { get; set; }
        public DateTime DateOfServiceStart { get; set; }

        public VehicleDto(Guid Id, string registration, string vin, string vehicleTypeCode, DateTime dateOfServiceStart)
        {
            this.Id = Id;
            this.Registration = registration;
            this.VIN = vin;
            this.VehicleTypeCode = vehicleTypeCode;
            this.DateOfServiceStart = dateOfServiceStart;
        }
    }
}
﻿using System;

namespace DDDSample1.Domain.Vehicles
{
    public class CreatingVehicleDto
    {
        public string Registration { get; set; }
        public string VIN { get; set; }
        public string VehicleTypeCode { get; set; }
        public DateTime DateOfServiceStart { get; private set; }

        public CreatingVehicleDto(string registration, string vin, string vehicleTypeCode, DateTime dateOfServiceStart)
        {
            this.Registration = registration;
            this.VIN = vin;
            this.VehicleTypeCode = vehicleTypeCode;
            this.DateOfServiceStart = dateOfServiceStart;
        }
    }
}
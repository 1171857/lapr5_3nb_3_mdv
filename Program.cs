﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

namespace DDDSample1
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args)
        {
            var port = System.Environment.GetEnvironmentVariable("PORT");


            if (port != null)
            {
                return WebHost.CreateDefaultBuilder(args)
                    .UseStartup<Startup>()
                    .UseUrls("http://*:" + port);
            } else
            {
                return WebHost.CreateDefaultBuilder(args)
                    .UseStartup<Startup>();
            }
        }
    }
}
